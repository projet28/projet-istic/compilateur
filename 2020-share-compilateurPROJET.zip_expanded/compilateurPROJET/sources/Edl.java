import java.io.InputStream;
import java.io.OutputStream;

 /**
 * 
 * @author Roz�_Malcolm, Jalam_Edouard, Rabe_C�saire
 * @version 2020
 *
 */


public class Edl {
	
	static class EltDef {
		// nomProc = nom de la procedure definie en DEF
		public String nomProc;
		// adPo = adresse de debut de code de cette procedure
		// nbParam =  nombre de parametres de cette procedure
		public int adPo, nbParam;

		public EltDef(String nomProc, int adPo, int nbParam) {
			this.nomProc = nomProc;
			this.adPo = adPo;
			this.nbParam = nbParam;
		}
	}

	// nombre max de modules, taille max d'un code objet d'une unite
	static final int MAXMOD = 5, MAXOBJ = 1000;
	// nombres max de references externes (REF) et de points d'entree (DEF)
	// pour une unite
	private static final int MAXREF = 10, MAXDEF = 10;

	// typologie des erreurs
	private static final int FATALE = 0, NONFATALE = 1;

	// valeurs possibles du vecteur de translation
	private static final int TRANSDON=1,TRANSCODE=2,REFEXT=3;

	// table de tous les descripteurs concernes par l'edl
	static Descripteur[] tabDesc = new Descripteur[MAXMOD + 1];

	static int ipo, nMod, nbErr;
	static String nomProg;
	static String[] nomMod;
	
	static int[] transDon;
	static int[] transCode;
	static EltDef[] dicoDef;
	static int[][] adFinale;


	// utilitaire de traitement des erreurs
	// ------------------------------------
	static void erreur(int te, String m) {
		System.out.println(m);
		if (te == FATALE) {
			System.out.println("ABANDON DE L'EDITION DE LIENS");
			System.exit(1);
		}
		nbErr = nbErr + 1;
	}

	// utilitaire de remplissage de la table des descripteurs tabDesc
	// --------------------------------------------------------------
	static void lireDescripteurs() {
		String s;
		System.out.println("les noms doivent etre fournis sans suffixe");
		System.out.print("nom du programme : ");
		s = Lecture.lireString();
		tabDesc[0] = new Descripteur();
		tabDesc[0].lireDesc(s);
		if (!tabDesc[0].getUnite().equals("programme"))
			erreur(FATALE, "programme attendu");
		nomProg = s;

		nomMod = new String[6];
		nomMod[nMod] = s;
		while (!s.equals("") && nMod < MAXMOD) {
			System.out.print("nom de module " + (nMod + 1)
					+ " (RC si termine) ");
			s = Lecture.lireString();
			if (!s.equals("")) {
				nMod++;
				nomMod[nMod] = s;
				tabDesc[nMod] = new Descripteur();
				tabDesc[nMod].lireDesc(s);
				
				if (!tabDesc[nMod].getUnite().equals("module"))
					erreur(FATALE, "module attendu");
			}
		}
	}


	static void constMap() {
		// f2 = fichier executable .map construit
		OutputStream f2 = Ecriture.ouvrir(nomProg + ".map");
		if (f2 == null)
			erreur(FATALE, "creation du fichier " + nomProg
					+ ".map impossible");
		// pour construire le code concatene de toutes les unit�s
		int[] po = new int[(nMod + 1) * MAXOBJ + 1];
		
		ipo = 1;
		
		for(int i=0; i<nMod+1; i++) {
			InputStream f1 = Lecture.ouvrir(nomMod[i] + ".obj");
			
			int[][] vTrans = new int[tabDesc[i].getNbTransExt()][2];
			
			for(int j=0; j<vTrans.length; j++) {
				String[] stmp = Lecture.lireString(f1).split("   ");
				vTrans[j][0] = Integer.parseInt(stmp[0]);
				vTrans[j][1] = Integer.parseInt(stmp[1]);
			}
			
			for(int j=0; j<tabDesc[i].getTailleCode(); j++) {
				po[ipo] = Lecture.lireInt(f1);
				ipo++;
			}
			
			for(int j=0; j<vTrans.length; j++) {
				switch(vTrans[j][1]) {
					case TRANSDON: po[vTrans[j][0]+transCode[i]] += transDon[i];
						break;
					case TRANSCODE: po[vTrans[j][0]+transCode[i]] += transCode[i];
						break;
					case REFEXT: po[vTrans[j][0]+transCode[i]] = adFinale[i][po[vTrans[j][0]]-1];
						break;
				}
			}
		}
		
		po[2] = transDon[nMod] + tabDesc[nMod].getTailleGlobaux();
		
		for (int i = 1; i < ipo; i++)
			Ecriture.ecrireStringln(f2, "" + po[i]);

		Ecriture.fermer(f2);

		// creation du fichier en mnemonique correspondant
		Mnemo.creerFichier(ipo-1, po, nomProg + ".ima");
	}

	public static void main(String argv[]) {
		System.out.println("EDITEUR DE LIENS / PROJET LICENCE");
		System.out.println("---------------------------------");
		System.out.println("");
		nbErr = 0;
		nMod = 0;
		
		int tmpVar = 0;
		int tmpTaille = 0;
		int cptDef = 0;
		int tmpDef;
		int tmpRef;
		
		// Phase 1 de l'edition de liens
		// -----------------------------
		lireDescripteurs();		
		
		transDon = new int[nMod+1];
		transCode = new int[nMod+1];

		for(int i=0; i<nMod+1; i++) {
			transDon[i] = tmpVar;
			tmpVar += tabDesc[i].getTailleGlobaux();
			
			transCode[i] = tmpTaille;
			tmpTaille += tabDesc[i].getTailleCode();
			
			cptDef += tabDesc[i].getNbDef();
		}
		
		dicoDef = new EltDef[cptDef];
		cptDef = 0;
		
		for(int i=0; i<nMod+1; i++) {
			tmpDef = tabDesc[i].getNbDef();
			if(tmpDef > MAXDEF) {
				erreur(NONFATALE, "nombre de DEF d�pass�");
				tmpDef = MAXDEF;
			}
			if(tmpDef < 1 && i > 0) erreur(NONFATALE, "module sans def (module inutile)");
			for(int j=1; j<tmpDef+1; j++) {
				for(int k=0; k<cptDef; k++) {
					if(dicoDef[k].nomProc == tabDesc[i].getDefNomProc(j)) {
						erreur(NONFATALE, "nom de DEF d�j� utilis� double definition " + tabDesc[i].getDefNomProc(j));
					}
				}
				dicoDef[cptDef] = new EltDef(tabDesc[i].getDefNomProc(j),
						tabDesc[i].getDefAdPo(j) + transCode[i], tabDesc[i].getDefNbParam(j));
				cptDef++;
			}
		}
		
		adFinale = new int[nMod+1][];
		
		for(int i=0; i<nMod+1; i++) {
			tmpRef = tabDesc[i].getNbRef();
			if(tmpRef > MAXREF) {
				erreur(NONFATALE, "nombre de REF d�pass�");
				tmpRef = MAXREF;
			}
			adFinale[i] = new int[tmpRef];
			for(int j=1; j<tmpRef+1; j++) {
				tmpDef=0;
				while(tmpDef < dicoDef.length && !tabDesc[i].getRefNomProc(j).equals(dicoDef[tmpDef].nomProc)) tmpDef++;
				
				if(tmpDef >= dicoDef.length) {
					erreur(NONFATALE, "cette ref " + tabDesc[i].getRefNomProc(j) + " n'a pas de def");
				}else {
				
					if(dicoDef[tmpDef].nbParam != tabDesc[i].getRefNbParam(j)) {
						erreur(NONFATALE, "nombre de param diff�rent pour la proc def et ref " + tabDesc[i].getRefNomProc(j));
					}
					adFinale[i][j-1] = dicoDef[tmpDef].adPo + transCode[i];
				}
			}
		}
		

		if (nbErr > 0) {
			System.out.println("programme executable non produit");
			System.exit(1);
		}

		// Phase 2 de l'edition de liens
		// -----------------------------
		constMap();				
		System.out.println("Edition de liens terminee");
	}
}
