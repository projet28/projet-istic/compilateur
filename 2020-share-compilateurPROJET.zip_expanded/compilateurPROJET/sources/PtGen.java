/*********************************************************************************
 * VARIABLES ET METHODES FOURNIES PAR LA CLASSE UtilLex (cf libClass_Projet)     *
 *       complement à l'ANALYSEUR LEXICAL produit par ANTLR                      *
 *                                                                               *
 *                                                                               *
 *   nom du programme compile, sans suffixe : String UtilLex.nomSource           *
 *   ------------------------                                                    *
 *                                                                               *
 *   attributs lexicaux (selon items figurant dans la grammaire):                *
 *   ------------------                                                          *
 *     int UtilLex.valEnt = valeur du dernier nombre entier lu (item nbentier)   *
 *     int UtilLex.numIdCourant = code du dernier identificateur lu (item ident) *
 *                                                                               *
 *                                                                               *
 *   methodes utiles :                                                           *
 *   ---------------                                                             *
 *     void UtilLex.messErr(String m)  affichage de m et arret compilation       *
 *     String UtilLex.chaineIdent(int numId) delivre l'ident de codage numId     *
 *     void afftabSymb()  affiche la table des symboles                          *
 *********************************************************************************/



/**
 * classe de mise en oeuvre du compilateur
 * =======================================
 * (verifications semantiques + production du code objet)
 * 
 * @author Girard, Grazon, Masson
 *
 */

public class PtGen {
   

    // constantes manipulees par le compilateur
    // ----------------------------------------

	private static final int 
	
	// taille max de la table des symboles
	MAXSYMB=300,

	// codes MAPILE :
	RESERVER=1,EMPILER=2,CONTENUG=3,AFFECTERG=4,OU=5,ET=6,NON=7,INF=8,
	INFEG=9,SUP=10,SUPEG=11,EG=12,DIFF=13,ADD=14,SOUS=15,MUL=16,DIV=17,
	BSIFAUX=18,BINCOND=19,LIRENT=20,LIREBOOL=21,ECRENT=22,ECRBOOL=23,
	ARRET=24,EMPILERADG=25,EMPILERADL=26,CONTENUL=27,AFFECTERL=28,
	APPEL=29,RETOUR=30,

	// codes des valeurs vrai/faux
	VRAI=1, FAUX=0,

    // types permis :
	ENT=1,BOOL=2,NEUTRE=3,

	// categories possibles des identificateurs :
	CONSTANTE=1,VARGLOBALE=2,VARLOCALE=3,PARAMFIXE=4,PARAMMOD=5,PROC=6,
	DEF=7,REF=8,PRIVEE=9,

    //valeurs possible du vecteur de translation 
    TRANSDON=1,TRANSCODE=2,REFEXT=3;


    // utilitaires de controle de type
    // -------------------------------
    /**
     * verification du type entier de l'expression en cours de compilation 
     * (arret de la compilation sinon)
     */
	private static void verifEnt() {
		if (tCour != ENT)
			UtilLex.messErr("expression entiere attendue");
	}
	/**
	 * verification du type booleen de l'expression en cours de compilation 
	 * (arret de la compilation sinon)
	 */
	private static void verifBool() {
		if (tCour != BOOL)
			UtilLex.messErr("expression booleenne attendue");
	}

    // pile pour gerer les chaines de reprise et les branchements en avant
    // -------------------------------------------------------------------

    private static TPileRep pileRep;  


    // production du code objet en memoire
    // -----------------------------------

    private static ProgObjet po;
    
    
    
    // COMPILATION SEPAREE 
    // -------------------
    //
    /** 
     * modification du vecteur de translation associe au code produit 
     * + incrementation attribut nbTransExt du descripteur
     *  NB: effectue uniquement si c'est une reference externe ou si on compile un module
     * @param valeur : TRANSDON, TRANSCODE ou REFEXT
     */
    private static void modifVecteurTrans(int valeur) {
		if (valeur == REFEXT || desc.getUnite().equals("module")) {
			po.vecteurTrans(valeur);
			desc.incrNbTansExt();
		}
	}    
    // descripteur associe a un programme objet (compilation separee)
    private static Descripteur desc;

     
    // autres variables fournies
    // -------------------------
    
 // MERCI de renseigner ici un nom pour le trinome, constitue EXCLUSIVEMENT DE LETTRES
    public static String trinome="ROZE_RABE_JALAM"; 
    
    private static int tCour; // type de l'expression compilee
    private static int vCour; // sert uniquement lors de la compilation d'une valeur (entiere ou boolenne)
    private static int tmptCour;
   
    // TABLE DES SYMBOLES
    // ------------------
    //
    private static EltTabSymb[] tabSymb = new EltTabSymb[MAXSYMB + 1];
    
    // it = indice de remplissage de tabSymb
    // bc = bloc courant (=1 si le bloc courant est le programme principal)
	private static int it, bc;
	
	private static int cptVar;
	private static int cptParam;
	private static int cptRef;
	private static int cptParamRef;
	
	private static int affectc;
	private static int affectv;
	private static int affectt;
	
	private static int ligneP;
	private static int paramP;
	private static int verifParamP;
	private static boolean ref;
	
	private static boolean VarGlobale;
	private static boolean declaration;
	private static boolean module;
	
	private static int tmp;
	private static int v;
	
	/** 
	 * utilitaire de recherche de l'ident courant (ayant pour code UtilLex.numIdCourant) dans tabSymb
	 * 
	 * @param borneInf : recherche de l'indice it vers borneInf (=1 si recherche dans tout tabSymb)
	 * @return : indice de l'ident courant (de code UtilLex.numIdCourant) dans tabSymb (O si absence)
	 */
	private static int presentIdent(int borneInf) {
		int i = it;
		while (i >= borneInf && tabSymb[i].code != UtilLex.numIdCourant)
			i--;
		if (i >= borneInf)
			return i;
		else
			return 0;
	}

	/**
	 * utilitaire de placement des caracteristiques d'un nouvel ident dans tabSymb
	 * 
	 * @param code : UtilLex.numIdCourant de l'ident
	 * @param cat : categorie de l'ident parmi CONSTANTE, VARGLOBALE, PROC, etc.
	 * @param type : ENT, BOOL ou NEUTRE
	 * @param info : valeur pour une constante, ad d'exécution pour une variable, etc.
	 */
	private static void placeIdent(int code, int cat, int type, int info) {
		if (it == MAXSYMB)
			UtilLex.messErr("debordement de la table des symboles");
		it = it + 1;
		tabSymb[it] = new EltTabSymb(code, cat, type, info);
	}

	/**
	 *  utilitaire d'affichage de la table des symboles
	 */
	private static void afftabSymb() { 
		System.out.println("       code           categorie      type    info");
		System.out.println("      |--------------|--------------|-------|----");
		for (int i = 1; i <= it; i++) {
			if (i == bc) {
				System.out.print("bc=");
				Ecriture.ecrireInt(i, 3);
			} else if (i == it) {
				System.out.print("it=");
				Ecriture.ecrireInt(i, 3);
			} else
				Ecriture.ecrireInt(i, 6);
			if (tabSymb[i] == null)
				System.out.println(" reference NULL");
			else
				System.out.println(" " + tabSymb[i]);
		}
		System.out.println();
	}
    

	/**
	 *  initialisations A COMPLETER SI BESOIN
	 *  -------------------------------------
	 */
	public static void initialisations() {
	
		// indices de gestion de la table des symboles
		it = 0;
		bc = 1;
		
		// pile des reprises pour compilation des branchements en avant
		pileRep = new TPileRep(); 
		// programme objet = code Mapile de l'unite en cours de compilation
		po = new ProgObjet();
		// COMPILATION SEPAREE: desripteur de l'unite en cours de compilation
		desc = new Descripteur();
		
		// initialisation necessaire aux attributs lexicaux
		UtilLex.initialisation();
	
		// initialisation du type de l'expression courante
		tCour = NEUTRE;
		
		cptVar = 0;
		cptParam = 0;
		cptRef = 0;
		cptParamRef = 0;
		
		VarGlobale = true;
		declaration = true;
		module = false;
		
		verifParamP = 0;
		
		tmp = 0;
		v = 0;

	} // initialisations

	/**
	 *  code des points de generation A COMPLETER
	 *  -----------------------------------------
	 * @param numGen : numero du point de generation a executer
	 */
	public static void pt(int numGen) {
		switch (numGen) {
		/** INITIALISATION **/
		case 0:
			initialisations();
			break;
			
		/** DECLARATION **/
		
		case 1: if(presentIdent(bc) == 0) {
					placeIdent(UtilLex.numIdCourant, CONSTANTE, 0, 0);
				}else {
					UtilLex.messErr("erreur nom de constante déjà attribuer");
				}
		break;
			
		case 2: tabSymb[it].info = vCour; 
				tabSymb[it].type = tCour;
		break;
		
		case 3: if(presentIdent(bc) == 0) {
					if(bc == 1) {
						placeIdent(UtilLex.numIdCourant, VARGLOBALE, tCour, cptVar);
					}else {
						placeIdent(UtilLex.numIdCourant, VARLOCALE, tCour, cptVar+cptParam+2);
					}
					cptVar++;
				}else {
					UtilLex.messErr("erreur nom de variable déjà attribuer");
				} 
		break;
		
		case 4: if(VarGlobale) {
					VarGlobale = false;
					desc.setTailleGlobaux(cptVar);
				}
				if(!module) {
					po.produire(RESERVER);
				    po.produire(cptVar);
				    cptVar = 0;
				}
		break;	
		
		case 5: tCour = ENT;
		break;
		
		case 6: tCour = BOOL;
		break;
		
		case 7: declaration = false;
		break;
		
		/** VERIFICATION **/
		
		case 11: verifEnt();
		break;
		
		case 12: verifBool();
		break;
		
		case 13: tmptCour = tCour;
		break;
		
		case 14: if(tCour != tmptCour) {
					  UtilLex.messErr("erreur les expressions n'ont pas le même type");
				 }
		break;
		
		/** AFFECTATION **/
		
		case 21: if((tmp = presentIdent(1)) != 0) {
					 if(tabSymb[tmp].categorie == PARAMFIXE || tabSymb[tmp].categorie == CONSTANTE) {
						 UtilLex.messErr("erreur impossible de modifier un paramfixe ou une constante");
					 }else {
						 affectc = tabSymb[tmp].categorie;
						 affectv = tabSymb[tmp].info;
						 affectt = tabSymb[tmp].type;
					 }
				 }else {
				 	 UtilLex.messErr("erreur variable inexistante");
				 }
		break;
		
		case 22: if(tCour == affectt) {
					if(affectc == VARGLOBALE) {
						po.produire(AFFECTERG);
					}else if(affectc == VARLOCALE || affectc == PARAMMOD) {
						po.produire(AFFECTERL);
					}
					po.produire(affectv);
					if(module && affectc == VARGLOBALE) modifVecteurTrans(TRANSDON);
					if(affectc == VARLOCALE) {
						po.produire(0);
					}
					if(affectc == PARAMMOD) {
						po.produire(1);
					}
				}else {
					UtilLex.messErr("erreur types : le type est différent");
				}
		break;
		
		/** LECTURE **/
		
		case 31: if((tmp = presentIdent(1)) != 0) {
					if(tabSymb[tmp].type == ENT) {
					   	 po.produire(LIRENT);
					 }else if(tabSymb[tmp].type == BOOL) {
					 	 po.produire(LIREBOOL);
					 }
					 if(tabSymb[tmp].categorie == VARGLOBALE) {
						 po.produire(AFFECTERG);
					 }else if(tabSymb[tmp].categorie == VARLOCALE || tabSymb[tmp].categorie == PARAMMOD) {
						 po.produire(AFFECTERL);
					}else {
						UtilLex.messErr("erreur catégorie : ce n'est pas une catégorie affectable");
					}
					po.produire(tabSymb[tmp].info);
					if(module && tabSymb[tmp].categorie == VARGLOBALE) modifVecteurTrans(TRANSDON);
					if(tabSymb[tmp].categorie == VARLOCALE) {
						 po.produire(0);
					 }
					 if(tabSymb[tmp].categorie == PARAMMOD) {
						 po.produire(1);
					 }
				}else {
					UtilLex.messErr("erreur : ident n'existe pas");
				}	
		break;
		
		/** ECRITURE **/
	
		case 41: if(tCour == ENT) po.produire(ECRENT);
				 if(tCour == BOOL) po.produire(ECRBOOL);
		break;
	
		/** INSSI **/
		
		case 50: pileRep.empiler(0);
		break;
		
		case 51: po.produire(BSIFAUX);
				 po.produire(0);
				 if(module) modifVecteurTrans(TRANSCODE);
				 pileRep.empiler(po.getIpo());
		break;
		
		case 52: v = pileRep.depiler();
				 po.modifier(v, po.getIpo()+1);
		break;
		
		case 53: po.modifier(v, po.getElt(v) + 2);
				 po.produire(BINCOND);
				 po.produire(pileRep.depiler());
				 if(module) modifVecteurTrans(TRANSCODE);
		 		 pileRep.empiler(po.getIpo());
		break;
		
		case 54: int i2 = pileRep.depiler();
				 while(i2 != 0) {
					int tmp2 = po.getElt(i2);
					po.modifier(i2, po.getIpo()+1);
					i2 = tmp2;
				 }
				 po.modifier(i2, po.getIpo()+1);
		break;
		
		/** INSCOND **/
		
		case 61: po.produire(BSIFAUX);
				 po.produire(0);
				 if(module) modifVecteurTrans(TRANSCODE);
				 pileRep.empiler(po.getIpo());
		break;
		
		case 62: v = pileRep.depiler();
				 po.modifier(v, po.getIpo()+1);
		break;
		
		case 63: pileRep.empiler(0);
		break;
		
		case 64: po.modifier(v, po.getElt(v)+2);
				 po.produire(BINCOND);
		 		 po.produire(pileRep.depiler());
		 		 if(module) modifVecteurTrans(TRANSCODE);
		 		 pileRep.empiler(po.getIpo());
		break;
		
		case 65: int i = pileRep.depiler();
				 while(i != 0) {
					int tmp2 = po.getElt(i);
					po.modifier(i, po.getIpo()+1);
					i = tmp2;
				 }
				 po.modifier(i, po.getIpo()+1);
		break;
		
		/** BOUCLE **/
		
		case 71: pileRep.empiler(po.getIpo()+1);
		break;
		
		case 72: po.produire(BSIFAUX);
				 po.produire(0);
				 if(module) modifVecteurTrans(TRANSCODE);
				 pileRep.empiler(po.getIpo());
		break;
		
		case 73: po.modifier(pileRep.depiler(), po.getIpo()+3);
		break;
		
		case 74: po.produire(BINCOND);
			     po.produire(pileRep.depiler());
			     if(module) modifVecteurTrans(TRANSCODE);
		break;
		
		/** EXPRESSION **/
		
		/** VALEUR, PRIMAIRE **/
		
		case 81: tCour = BOOL;
				 if(declaration) {
					 vCour = FAUX;
				 }else {
					 po.produire(EMPILER);
					 po.produire(FAUX);
				 }
		break;
		case 82: tCour = BOOL;
				if(declaration) {
					vCour = VRAI;
				}else {
					po.produire(EMPILER);
					po.produire(VRAI);
				}
		break;
		case 83: tCour = ENT;
				if(declaration) {
					vCour = -UtilLex.valEnt;
				}else {
					po.produire(EMPILER);
					po.produire(-UtilLex.valEnt);
				}
			break;
		case 84: tCour = ENT;
				if(declaration) {
					vCour = UtilLex.valEnt;
				}else {
					po.produire(EMPILER);
					po.produire(UtilLex.valEnt);
				}
			break;
		case 85: 
			if((tmp = presentIdent(1)) != 0) {
				if(tabSymb[tmp].categorie == VARGLOBALE) {
					po.produire(CONTENUG);
				}else if(tabSymb[tmp].categorie == CONSTANTE) {
					po.produire(EMPILER);
				}else if(tabSymb[tmp].categorie == VARLOCALE || tabSymb[tmp].categorie == PARAMFIXE ||
						tabSymb[tmp].categorie == PARAMMOD){
					po.produire(CONTENUL);
				}
				po.produire(tabSymb[tmp].info);
				if(module && tabSymb[tmp].categorie == VARGLOBALE) modifVecteurTrans(TRANSDON);
				if(tabSymb[tmp].categorie == VARLOCALE || tabSymb[tmp].categorie == PARAMFIXE) {
					po.produire(0);
				}
				if(tabSymb[tmp].categorie == PARAMMOD) {
					po.produire(1);
				}
				tCour = tabSymb[tmp].type;
			}else {
				UtilLex.messErr("erreur variable non attribuer");
			}
			break;
		
		/** OPERATION **/
		
		case 91: po.produire(DIV);
				tCour = ENT;
		break;
		
		case 92: po.produire(MUL);
				tCour = ENT;
		break;
		
		case 93: po.produire(SOUS);
				tCour = ENT;
		break;
		
		case 94: po.produire(ADD);
				 tCour = ENT;
		break;	
			
		/** COMPARAISON **/
		
		case 101: po.produire(INFEG);
		 		 tCour = BOOL;
		break;
		
		case 102: po.produire(INF);
				 tCour = BOOL;
		break;
		
		case 103: po.produire(SUPEG);
				 tCour = BOOL;
		break;
		
		case 104: po.produire(SUP);
				 tCour = BOOL;
		break;
		
		case 105: po.produire(DIFF);
			     tCour = BOOL;
		break;
		
		case 106: po.produire(EG);
				 tCour = BOOL;
		break;
		
		/** LOGIQUE **/
		
		case 111: po.produire(NON);
				 tCour = BOOL;
			break;
		case 112: po.produire(ET);
				 tCour = BOOL;
			break;	
		case 113: po.produire(OU);
				 tCour = BOOL;
			break;
			
			
		/** PROC **/
			
		/** SAUT PROC **/
		
		case 121 : if(!module) {
					   po.produire(BINCOND);
					   po.produire(0);
					   pileRep.empiler(po.getIpo());
				   }
			break;
			
		case 122 : if(!module) po.modifier(pileRep.depiler(), po.getIpo()+1);
			break;
			
		/** DECLARATION PROC **/	
			
		case 131: if(presentIdent(1) == 0) {
				 	  placeIdent(UtilLex.numIdCourant, PROC, NEUTRE, po.getIpo()+1);
					  if((tmp = desc.presentDef(UtilLex.chaineIdent(UtilLex.numIdCourant))) != 0) {
						  desc.modifDefAdPo(tmp, po.getIpo()+1);
						  placeIdent(-1, DEF, NEUTRE, 0);
					  }else {
						  placeIdent(-1, PRIVEE, NEUTRE, 0);
					  }
					  bc = it+1;
				  }else {
					  UtilLex.messErr("erreur nom de proc déjà pris");
				  }
			break;
			
		case 132: declaration = true;
				  tabSymb[bc-1].info = cptParam;
				  desc.modifDefNbParam(tmp, cptParam);
			break;
			
		case 133: po.produire(RETOUR);
				  po.produire(cptParam);
				  int nb = bc;
				  while(nb != bc+cptParam){
					  tabSymb[nb].code = -1;
					  nb++;
				  }
				  it = nb-1;
				  cptParam = 0;
				  bc = 1;
			break;
			
	/** PARAMFIXE PARAMMOD **/
			
		case 141: if(presentIdent(bc) == 0) {
					  placeIdent(UtilLex.numIdCourant, PARAMFIXE, tCour, cptParam);
					  cptParam++;
				  }else {
					  UtilLex.messErr("nom de paramètre déjà utilisé");
				  }
			break;
		
		case 142: if(presentIdent(bc) == 0) {
					  placeIdent(UtilLex.numIdCourant, PARAMMOD, tCour, cptParam);
					  cptParam++;
				  }else {
					  UtilLex.messErr("nom de paramètre déjà utilisé");
				  }
			break;
			
	/** APPEL **/
		
		case 151: if((tmp = presentIdent(1)) != 0) {
				  		ligneP = tabSymb[tmp].info;
				  		paramP = tabSymb[tmp+1].info;
				  		ref = tabSymb[tmp+1].categorie == REF;
				  		verifParamP = 0;
				  }else {
					  UtilLex.messErr("erreur : cette proc n'existe pas");
				  }
			break;
			
		case 152: po.produire(APPEL);
				  po.produire(ligneP);
				  if(module) modifVecteurTrans(TRANSCODE);
				  if(ref) modifVecteurTrans(REFEXT);
				  if(paramP!=verifParamP) UtilLex.messErr("nombre de paramètre non respecter");
				  po.produire(paramP);
			break;
			
	/** EMPILER **/
			
		case 161: if((tmp = presentIdent(1)) != 0) {
				 		if(tabSymb[tmp].categorie == VARGLOBALE) {
				 			po.produire(EMPILERADG);
				 		}else if(tabSymb[tmp].categorie == VARLOCALE || tabSymb[tmp].categorie == PARAMMOD) {
				 			po.produire(EMPILERADL);
				 		}else {
				 			UtilLex.messErr("erreur : mauvaise catégorie du paramètre");
				 		}
				 		po.produire(tabSymb[tmp].info);
				 		if(module && tabSymb[tmp].categorie == VARGLOBALE) modifVecteurTrans(TRANSDON);
			 			if(tabSymb[tmp].categorie == VARLOCALE) {
			 				po.produire(0);
			 			}
			 			if(tabSymb[tmp].categorie == PARAMMOD) {
			 				po.produire(1);
			 			}
			 			tCour = tabSymb[tmp].type;
			 	  }else {
			 			UtilLex.messErr("erreur : c'est ident n'existe pas");
			 	  }
			break;
			
		case 162: verifParamP++;
			break;
			
	/** MODULE **/
			
		case 170: desc.setUnite("programme");
			break;
			
		case 171: desc.setUnite("module");
				  module = true;
			break;
			
		case 172: if(desc.presentDef(UtilLex.chaineIdent(UtilLex.numIdCourant)) == 0) {
				  	  desc.ajoutDef(UtilLex.chaineIdent(UtilLex.numIdCourant));
				  }else {
					  UtilLex.messErr("nom def déjà utilisé");
				  }
			break;
		
		case 173: if(desc.presentRef(UtilLex.chaineIdent(UtilLex.numIdCourant)) == 0) {
					   desc.ajoutRef(UtilLex.chaineIdent(UtilLex.numIdCourant));
					   cptRef++;
					   placeIdent(UtilLex.numIdCourant, PROC, NEUTRE, cptRef);
					   cptParamRef = 0;
				  }else {
					  UtilLex.messErr("nom ref déjà utilisé");
				  }
			break;
		
		case 174: cptParamRef++;
			break;
			
		case 175: cptParamRef++;
			break;
			
		case 176: desc.modifRefNbParam(cptRef, cptParamRef);
				  placeIdent(-1, REF, NEUTRE, cptParamRef);
			break;
		
	/** FIN **/
		
		case 999: if(!module) po.produire(ARRET);
				  desc.setTailleCode(po.getIpo());
				  afftabSymb();
				  System.out.println(desc.toString());
				  desc.ecrireDesc(UtilLex.nomSource);
				  po.constGen();
				  po.constObj();
				  break;
		
		default:
			System.out.println("Point de generation non prevu :" + numGen);
			break;

		}
	}
}
    
    
    
    
    
    
    
    
    
    
    
    
    
 
