// Grammaire du langage PROJET
// CMPL L3info 
// Nathalie Girard, Anne Grazon, Veronique Masson
// il convient d'y inserer les appels a {PtGen.pt(k);}
// relancer Antlr apres chaque modification et raffraichir le projet Eclipse le cas echeant

// attention l'analyse est poursuivie apres erreur si l'on supprime la clause rulecatch

grammar projet;

options {
  language=Java; k=1;
 }

@header {           
import java.io.IOException;
import java.io.DataInputStream;
import java.io.FileInputStream;
} 


// partie syntaxique :  descripAtion de la grammaire //
// les non-terminaux doivent commencer par une minuscule


@members {

 
// variables globales et methodes utiles a placer ici
  
}
// la directive rulecatch permet d'interrompre l'analyse a la premiere erreur de syntaxe
@rulecatch {
catch (RecognitionException e) {reportError (e) ; throw e ; }}


unite  :   unitprog EOF 
      |    unitmodule EOF 
  ;
  
unitprog
  : 'programme' ident ':' 
  	 {PtGen.pt(170);} 
     declarations  
     corps { System.out.println("succes, arret de la compilation "); PtGen.pt(999);}  
  ;
  
unitmodule
  : 'module' ident ':'
  	 {PtGen.pt(171);} 
     declarations { System.out.println("succes, arret de la compilation du module"); PtGen.pt(999);} 
  ;
  
declarations
  : partiedef? partieref? consts? vars? decprocs? 
  ;
  
partiedef
  : 'def' ident {PtGen.pt(172);} (',' ident {PtGen.pt(172);} )* ptvg
  ;
  
partieref: 'ref'  specif {PtGen.pt(176);} (',' specif {PtGen.pt(176);} )* ptvg
  ;
  
specif  : ident {PtGen.pt(173);} ( 'fixe' '(' type {PtGen.pt(174);} ( ',' type {PtGen.pt(174);} )* ')' )? 
                 ( 'mod'  '(' type {PtGen.pt(175);} ( ',' type {PtGen.pt(175);} )* ')' )? 
  ;
  
consts  : 'const' ( ident {PtGen.pt(1);} '=' valeur {PtGen.pt(2);} ptvg  )+ 
  ;
  
vars  : 'var' ( type ident {PtGen.pt(3);} ( ','  ident {PtGen.pt(3);} )* ptvg  )+ {PtGen.pt(4);}
  ;
  
type  : 'ent' {PtGen.pt(5);}
  |     'bool' {PtGen.pt(6);}
  ;
  
decprocs: {PtGen.pt(121);} (decproc ptvg)+ {PtGen.pt(122);}
  ;
  
decproc :  'proc'  ident {PtGen.pt(131);} parfixe? parmod? {PtGen.pt(132);} consts? vars? corps {PtGen.pt(133);}
  ;
  
ptvg  : ';'
  | 
  ;
  
corps : 'debut' {PtGen.pt(7);} instructions 'fin'
  ;
  
parfixe: 'fixe' '(' pf ( ';' pf)* ')'
  ;
  
pf  : type ident {PtGen.pt(141);} ( ',' ident {PtGen.pt(141);} )*  
  ;

parmod  : 'mod' '(' pm ( ';' pm)* ')'
  ;
  
pm  : type ident {PtGen.pt(142);} ( ',' ident {PtGen.pt(142);} )*
  ;
  
instructions
  : instruction ( ';' instruction)*
  ;
  
instruction
  : inssi
  | inscond
  | boucle
  | lecture
  | ecriture
  | affouappel
  | 
  ;
  
inssi : 'si'{PtGen.pt(50);} expression {PtGen.pt(12); PtGen.pt(51);} 'alors' instructions  {PtGen.pt(52);}
		({PtGen.pt(53);}'sinonsi' expression {PtGen.pt(12); PtGen.pt(51);} 'alors' instructions  {PtGen.pt(52);})*
		({PtGen.pt(53);}'sinon' instructions )? {PtGen.pt(54);} 'fsi' 
  ;
  
inscond : 'cond' {PtGen.pt(63);} expression {PtGen.pt(12); PtGen.pt(61);} ':' instructions {PtGen.pt(62);}
          (',' {PtGen.pt(64);} expression {PtGen.pt(12); PtGen.pt(61);} ':' instructions {PtGen.pt(62);} )* 
          ({PtGen.pt(64);} 'aut'  instructions |  ) {PtGen.pt(65);} 
          'fcond'
  ;
  
boucle  : 'ttq' {PtGen.pt(71);} expression {PtGen.pt(12); PtGen.pt(72);} 'faire' instructions {PtGen.pt(73);} 'fait' {PtGen.pt(74);} 
  ;
  
lecture: 'lire' '(' ident {PtGen.pt(31);} ( ',' ident {PtGen.pt(31);} )* ')' 
  ;
  
ecriture: 'ecrire' '(' expression {PtGen.pt(41);} ( ',' expression {PtGen.pt(41);} )* ')'
   ;
  
affouappel
  : ident  ( {PtGen.pt(21);} ':=' expression {PtGen.pt(22);}
            |  {PtGen.pt(151);} (effixes {PtGen.pt(162);} (effmods {PtGen.pt(162);} )?)? {PtGen.pt(152);} 
           )
  ;
  
effixes : '(' (expression  (',' expression  )*)? ')'
  ;
  
effmods :'(' (ident {PtGen.pt(161);} (',' ident {PtGen.pt(161);} )*)? ')'
  ; 
  
expression: (exp1) ('ou' {PtGen.pt(12);} exp1 {PtGen.pt(12); PtGen.pt(113);} )*
  ;
  
exp1  : exp2 ('et' {PtGen.pt(12);} exp2 {PtGen.pt(12); PtGen.pt(112);} )*
  ;
  
exp2  : 'non' exp2 {PtGen.pt(12);} {PtGen.pt(111);}
  | exp3  
  ;
  
exp3  : exp4 
  ( '=' {PtGen.pt(13);}  exp4 {PtGen.pt(14); PtGen.pt(106);}
  | '<>' {PtGen.pt(13);} exp4 {PtGen.pt(14); PtGen.pt(105);}
  | '>' {PtGen.pt(11);}  exp4 {PtGen.pt(11); PtGen.pt(104);}
  | '>=' {PtGen.pt(11);} exp4 {PtGen.pt(11); PtGen.pt(103);}
  | '<'  {PtGen.pt(11);} exp4 {PtGen.pt(11); PtGen.pt(102);}
  | '<=' {PtGen.pt(11);} exp4 {PtGen.pt(11); PtGen.pt(101);} 
  ) ?
  ;
  
exp4  : exp5 
        ('+' {PtGen.pt(11);}  exp5 {PtGen.pt(11); PtGen.pt(94);}
        |'-' {PtGen.pt(11);}  exp5 {PtGen.pt(11); PtGen.pt(93);}
        )*
  ;
  
exp5  : primaire 
        (    '*' {PtGen.pt(11);} primaire {PtGen.pt(11); PtGen.pt(92);}
          | 'div' {PtGen.pt(11);}  primaire {PtGen.pt(11); PtGen.pt(91);}
        )*
  ;
  
primaire: valeur 
  | ident {PtGen.pt(85);} 
  | '('  expression ')'
  ;
  
valeur  : nbentier {PtGen.pt(84);}
  | '+' nbentier {PtGen.pt(84);}
  | '-' nbentier {PtGen.pt(83);}
  | 'vrai' {PtGen.pt(82);}
  | 'faux' {PtGen.pt(81);}
  ;
  
 

// partie lexicale  : cette partie ne doit pas etre modifiee  //
// les unites lexicales de ANTLR doivent commencer par une majuscule
// Attention : ANTLR n'autorise pas certains traitements sur les unites lexicales, 
// il est alors ncessaire de passer par un non-terminal intermediaire 
// exemple : pour l'unit lexicale INT, le non-terminal nbentier a du etre introduit
 
      
nbentier  :   INT { UtilLex.valEnt = Integer.parseInt($INT.text);}; // mise a jour de valEnt

ident : ID  { UtilLex.traiterId($ID.text); } ; // mise a jour de numIdCourant
     // tous les identificateurs seront places dans la table des identificateurs, y compris le nom du programme ou module
     // (NB: la table des symboles n'est pas geree au niveau lexical mais au niveau du compilateur)
        
  
ID  :   ('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ; 
     
// zone purement lexicale //

INT :   '0'..'9'+ ;
WS  :   (' '|'\t' |'\r')+ {skip();} ; // definition des "blocs d'espaces"
RC  :   ('\n') {UtilLex.incrementeLigne(); skip() ;} ; // definition d'un unique "passage a la ligne" et comptage des numeros de lignes

COMMENT
  :  '\{' (.)* '\}' {skip();}   // toute suite de caracteres entouree d'accolades est un commentaire
  |  '#' ~( '\r' | '\n' )* {skip();}  // tout ce qui suit un caractere diese sur une ligne est un commentaire
  ;

// commentaires sur plusieurs lignes
ML_COMMENT    :   '/*' (options {greedy=false;} : .)* '*/' {$channel=HIDDEN;}
    ;	   



	   
