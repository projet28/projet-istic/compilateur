// $ANTLR 3.5.2 projet.g 2020-04-15 20:05:49
           
import java.io.IOException;
import java.io.DataInputStream;
import java.io.FileInputStream;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class projetParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "ID", "INT", "ML_COMMENT", 
		"RC", "WS", "'('", "')'", "'*'", "'+'", "','", "'-'", "':'", "':='", "';'", 
		"'<'", "'<='", "'<>'", "'='", "'>'", "'>='", "'alors'", "'aut'", "'bool'", 
		"'cond'", "'const'", "'debut'", "'def'", "'div'", "'ecrire'", "'ent'", 
		"'et'", "'faire'", "'fait'", "'faux'", "'fcond'", "'fin'", "'fixe'", "'fsi'", 
		"'lire'", "'mod'", "'module'", "'non'", "'ou'", "'proc'", "'programme'", 
		"'ref'", "'si'", "'sinon'", "'sinonsi'", "'ttq'", "'var'", "'vrai'"
	};
	public static final int EOF=-1;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int T__12=12;
	public static final int T__13=13;
	public static final int T__14=14;
	public static final int T__15=15;
	public static final int T__16=16;
	public static final int T__17=17;
	public static final int T__18=18;
	public static final int T__19=19;
	public static final int T__20=20;
	public static final int T__21=21;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int T__24=24;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int T__28=28;
	public static final int T__29=29;
	public static final int T__30=30;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int COMMENT=4;
	public static final int ID=5;
	public static final int INT=6;
	public static final int ML_COMMENT=7;
	public static final int RC=8;
	public static final int WS=9;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public projetParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public projetParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return projetParser.tokenNames; }
	@Override public String getGrammarFileName() { return "projet.g"; }



	 
	// variables globales et methodes utiles a placer ici
	  



	// $ANTLR start "unite"
	// projet.g:37:1: unite : ( unitprog EOF | unitmodule EOF );
	public final void unite() throws RecognitionException {
		try {
			// projet.g:37:8: ( unitprog EOF | unitmodule EOF )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==49) ) {
				alt1=1;
			}
			else if ( (LA1_0==45) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// projet.g:37:12: unitprog EOF
					{
					pushFollow(FOLLOW_unitprog_in_unite64);
					unitprog();
					state._fsp--;

					match(input,EOF,FOLLOW_EOF_in_unite66); 
					}
					break;
				case 2 :
					// projet.g:38:12: unitmodule EOF
					{
					pushFollow(FOLLOW_unitmodule_in_unite80);
					unitmodule();
					state._fsp--;

					match(input,EOF,FOLLOW_EOF_in_unite82); 
					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "unite"



	// $ANTLR start "unitprog"
	// projet.g:41:1: unitprog : 'programme' ident ':' declarations corps ;
	public final void unitprog() throws RecognitionException {
		try {
			// projet.g:42:3: ( 'programme' ident ':' declarations corps )
			// projet.g:42:5: 'programme' ident ':' declarations corps
			{
			match(input,49,FOLLOW_49_in_unitprog98); 
			pushFollow(FOLLOW_ident_in_unitprog100);
			ident();
			state._fsp--;

			match(input,16,FOLLOW_16_in_unitprog102); 
			PtGen.pt(170);
			pushFollow(FOLLOW_declarations_in_unitprog117);
			declarations();
			state._fsp--;

			pushFollow(FOLLOW_corps_in_unitprog126);
			corps();
			state._fsp--;

			 System.out.println("succes, arret de la compilation "); PtGen.pt(999);
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "unitprog"



	// $ANTLR start "unitmodule"
	// projet.g:48:1: unitmodule : 'module' ident ':' declarations ;
	public final void unitmodule() throws RecognitionException {
		try {
			// projet.g:49:3: ( 'module' ident ':' declarations )
			// projet.g:49:5: 'module' ident ':' declarations
			{
			match(input,45,FOLLOW_45_in_unitmodule145); 
			pushFollow(FOLLOW_ident_in_unitmodule147);
			ident();
			state._fsp--;

			match(input,16,FOLLOW_16_in_unitmodule149); 
			PtGen.pt(171);
			pushFollow(FOLLOW_declarations_in_unitmodule163);
			declarations();
			state._fsp--;

			 System.out.println("succes, arret de la compilation du module"); PtGen.pt(999);
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "unitmodule"



	// $ANTLR start "declarations"
	// projet.g:54:1: declarations : ( partiedef )? ( partieref )? ( consts )? ( vars )? ( decprocs )? ;
	public final void declarations() throws RecognitionException {
		try {
			// projet.g:55:3: ( ( partiedef )? ( partieref )? ( consts )? ( vars )? ( decprocs )? )
			// projet.g:55:5: ( partiedef )? ( partieref )? ( consts )? ( vars )? ( decprocs )?
			{
			// projet.g:55:5: ( partiedef )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==31) ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// projet.g:55:5: partiedef
					{
					pushFollow(FOLLOW_partiedef_in_declarations181);
					partiedef();
					state._fsp--;

					}
					break;

			}

			// projet.g:55:16: ( partieref )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==50) ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// projet.g:55:16: partieref
					{
					pushFollow(FOLLOW_partieref_in_declarations184);
					partieref();
					state._fsp--;

					}
					break;

			}

			// projet.g:55:27: ( consts )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==29) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// projet.g:55:27: consts
					{
					pushFollow(FOLLOW_consts_in_declarations187);
					consts();
					state._fsp--;

					}
					break;

			}

			// projet.g:55:35: ( vars )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==55) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// projet.g:55:35: vars
					{
					pushFollow(FOLLOW_vars_in_declarations190);
					vars();
					state._fsp--;

					}
					break;

			}

			// projet.g:55:41: ( decprocs )?
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==48) ) {
				alt6=1;
			}
			switch (alt6) {
				case 1 :
					// projet.g:55:41: decprocs
					{
					pushFollow(FOLLOW_decprocs_in_declarations193);
					decprocs();
					state._fsp--;

					}
					break;

			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "declarations"



	// $ANTLR start "partiedef"
	// projet.g:58:1: partiedef : 'def' ident ( ',' ident )* ptvg ;
	public final void partiedef() throws RecognitionException {
		try {
			// projet.g:59:3: ( 'def' ident ( ',' ident )* ptvg )
			// projet.g:59:5: 'def' ident ( ',' ident )* ptvg
			{
			match(input,31,FOLLOW_31_in_partiedef210); 
			pushFollow(FOLLOW_ident_in_partiedef212);
			ident();
			state._fsp--;

			PtGen.pt(172);
			// projet.g:59:34: ( ',' ident )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==14) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// projet.g:59:35: ',' ident
					{
					match(input,14,FOLLOW_14_in_partiedef217); 
					pushFollow(FOLLOW_ident_in_partiedef219);
					ident();
					state._fsp--;

					PtGen.pt(172);
					}
					break;

				default :
					break loop7;
				}
			}

			pushFollow(FOLLOW_ptvg_in_partiedef226);
			ptvg();
			state._fsp--;

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "partiedef"



	// $ANTLR start "partieref"
	// projet.g:62:1: partieref : 'ref' specif ( ',' specif )* ptvg ;
	public final void partieref() throws RecognitionException {
		try {
			// projet.g:62:10: ( 'ref' specif ( ',' specif )* ptvg )
			// projet.g:62:12: 'ref' specif ( ',' specif )* ptvg
			{
			match(input,50,FOLLOW_50_in_partieref238); 
			pushFollow(FOLLOW_specif_in_partieref241);
			specif();
			state._fsp--;

			PtGen.pt(176);
			// projet.g:62:43: ( ',' specif )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0==14) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// projet.g:62:44: ',' specif
					{
					match(input,14,FOLLOW_14_in_partieref246); 
					pushFollow(FOLLOW_specif_in_partieref248);
					specif();
					state._fsp--;

					PtGen.pt(176);
					}
					break;

				default :
					break loop8;
				}
			}

			pushFollow(FOLLOW_ptvg_in_partieref255);
			ptvg();
			state._fsp--;

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "partieref"



	// $ANTLR start "specif"
	// projet.g:65:1: specif : ident ( 'fixe' '(' type ( ',' type )* ')' )? ( 'mod' '(' type ( ',' type )* ')' )? ;
	public final void specif() throws RecognitionException {
		try {
			// projet.g:65:9: ( ident ( 'fixe' '(' type ( ',' type )* ')' )? ( 'mod' '(' type ( ',' type )* ')' )? )
			// projet.g:65:11: ident ( 'fixe' '(' type ( ',' type )* ')' )? ( 'mod' '(' type ( ',' type )* ')' )?
			{
			pushFollow(FOLLOW_ident_in_specif269);
			ident();
			state._fsp--;

			PtGen.pt(173);
			// projet.g:65:34: ( 'fixe' '(' type ( ',' type )* ')' )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0==41) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// projet.g:65:36: 'fixe' '(' type ( ',' type )* ')'
					{
					match(input,41,FOLLOW_41_in_specif275); 
					match(input,10,FOLLOW_10_in_specif277); 
					pushFollow(FOLLOW_type_in_specif279);
					type();
					state._fsp--;

					PtGen.pt(174);
					// projet.g:65:69: ( ',' type )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==14) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// projet.g:65:71: ',' type
							{
							match(input,14,FOLLOW_14_in_specif285); 
							pushFollow(FOLLOW_type_in_specif287);
							type();
							state._fsp--;

							PtGen.pt(174);
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,11,FOLLOW_11_in_specif294); 
					}
					break;

			}

			// projet.g:66:18: ( 'mod' '(' type ( ',' type )* ')' )?
			int alt12=2;
			int LA12_0 = input.LA(1);
			if ( (LA12_0==44) ) {
				alt12=1;
			}
			switch (alt12) {
				case 1 :
					// projet.g:66:20: 'mod' '(' type ( ',' type )* ')'
					{
					match(input,44,FOLLOW_44_in_specif319); 
					match(input,10,FOLLOW_10_in_specif322); 
					pushFollow(FOLLOW_type_in_specif324);
					type();
					state._fsp--;

					PtGen.pt(175);
					// projet.g:66:53: ( ',' type )*
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( (LA11_0==14) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// projet.g:66:55: ',' type
							{
							match(input,14,FOLLOW_14_in_specif330); 
							pushFollow(FOLLOW_type_in_specif332);
							type();
							state._fsp--;

							PtGen.pt(175);
							}
							break;

						default :
							break loop11;
						}
					}

					match(input,11,FOLLOW_11_in_specif339); 
					}
					break;

			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "specif"



	// $ANTLR start "consts"
	// projet.g:69:1: consts : 'const' ( ident '=' valeur ptvg )+ ;
	public final void consts() throws RecognitionException {
		try {
			// projet.g:69:9: ( 'const' ( ident '=' valeur ptvg )+ )
			// projet.g:69:11: 'const' ( ident '=' valeur ptvg )+
			{
			match(input,29,FOLLOW_29_in_consts357); 
			// projet.g:69:19: ( ident '=' valeur ptvg )+
			int cnt13=0;
			loop13:
			while (true) {
				int alt13=2;
				int LA13_0 = input.LA(1);
				if ( (LA13_0==ID) ) {
					alt13=1;
				}

				switch (alt13) {
				case 1 :
					// projet.g:69:21: ident '=' valeur ptvg
					{
					pushFollow(FOLLOW_ident_in_consts361);
					ident();
					state._fsp--;

					PtGen.pt(1);
					match(input,22,FOLLOW_22_in_consts365); 
					pushFollow(FOLLOW_valeur_in_consts367);
					valeur();
					state._fsp--;

					PtGen.pt(2);
					pushFollow(FOLLOW_ptvg_in_consts371);
					ptvg();
					state._fsp--;

					}
					break;

				default :
					if ( cnt13 >= 1 ) break loop13;
					EarlyExitException eee = new EarlyExitException(13, input);
					throw eee;
				}
				cnt13++;
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "consts"



	// $ANTLR start "vars"
	// projet.g:72:1: vars : 'var' ( type ident ( ',' ident )* ptvg )+ ;
	public final void vars() throws RecognitionException {
		try {
			// projet.g:72:7: ( 'var' ( type ident ( ',' ident )* ptvg )+ )
			// projet.g:72:9: 'var' ( type ident ( ',' ident )* ptvg )+
			{
			match(input,55,FOLLOW_55_in_vars390); 
			// projet.g:72:15: ( type ident ( ',' ident )* ptvg )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==27||LA15_0==34) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// projet.g:72:17: type ident ( ',' ident )* ptvg
					{
					pushFollow(FOLLOW_type_in_vars394);
					type();
					state._fsp--;

					pushFollow(FOLLOW_ident_in_vars396);
					ident();
					state._fsp--;

					PtGen.pt(3);
					// projet.g:72:43: ( ',' ident )*
					loop14:
					while (true) {
						int alt14=2;
						int LA14_0 = input.LA(1);
						if ( (LA14_0==14) ) {
							alt14=1;
						}

						switch (alt14) {
						case 1 :
							// projet.g:72:45: ',' ident
							{
							match(input,14,FOLLOW_14_in_vars402); 
							pushFollow(FOLLOW_ident_in_vars405);
							ident();
							state._fsp--;

							PtGen.pt(3);
							}
							break;

						default :
							break loop14;
						}
					}

					pushFollow(FOLLOW_ptvg_in_vars412);
					ptvg();
					state._fsp--;

					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			PtGen.pt(4);
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "vars"



	// $ANTLR start "type"
	// projet.g:75:1: type : ( 'ent' | 'bool' );
	public final void type() throws RecognitionException {
		try {
			// projet.g:75:7: ( 'ent' | 'bool' )
			int alt16=2;
			int LA16_0 = input.LA(1);
			if ( (LA16_0==34) ) {
				alt16=1;
			}
			else if ( (LA16_0==27) ) {
				alt16=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 16, 0, input);
				throw nvae;
			}

			switch (alt16) {
				case 1 :
					// projet.g:75:9: 'ent'
					{
					match(input,34,FOLLOW_34_in_type432); 
					PtGen.pt(5);
					}
					break;
				case 2 :
					// projet.g:76:9: 'bool'
					{
					match(input,27,FOLLOW_27_in_type444); 
					PtGen.pt(6);
					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "type"



	// $ANTLR start "decprocs"
	// projet.g:79:1: decprocs : ( decproc ptvg )+ ;
	public final void decprocs() throws RecognitionException {
		try {
			// projet.g:79:9: ( ( decproc ptvg )+ )
			// projet.g:79:11: ( decproc ptvg )+
			{
			PtGen.pt(121);
			// projet.g:79:28: ( decproc ptvg )+
			int cnt17=0;
			loop17:
			while (true) {
				int alt17=2;
				int LA17_0 = input.LA(1);
				if ( (LA17_0==48) ) {
					alt17=1;
				}

				switch (alt17) {
				case 1 :
					// projet.g:79:29: decproc ptvg
					{
					pushFollow(FOLLOW_decproc_in_decprocs461);
					decproc();
					state._fsp--;

					pushFollow(FOLLOW_ptvg_in_decprocs463);
					ptvg();
					state._fsp--;

					}
					break;

				default :
					if ( cnt17 >= 1 ) break loop17;
					EarlyExitException eee = new EarlyExitException(17, input);
					throw eee;
				}
				cnt17++;
			}

			PtGen.pt(122);
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "decprocs"



	// $ANTLR start "decproc"
	// projet.g:82:1: decproc : 'proc' ident ( parfixe )? ( parmod )? ( consts )? ( vars )? corps ;
	public final void decproc() throws RecognitionException {
		try {
			// projet.g:82:9: ( 'proc' ident ( parfixe )? ( parmod )? ( consts )? ( vars )? corps )
			// projet.g:82:12: 'proc' ident ( parfixe )? ( parmod )? ( consts )? ( vars )? corps
			{
			match(input,48,FOLLOW_48_in_decproc481); 
			pushFollow(FOLLOW_ident_in_decproc484);
			ident();
			state._fsp--;

			PtGen.pt(131);
			// projet.g:82:43: ( parfixe )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==41) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// projet.g:82:43: parfixe
					{
					pushFollow(FOLLOW_parfixe_in_decproc488);
					parfixe();
					state._fsp--;

					}
					break;

			}

			// projet.g:82:52: ( parmod )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==44) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// projet.g:82:52: parmod
					{
					pushFollow(FOLLOW_parmod_in_decproc491);
					parmod();
					state._fsp--;

					}
					break;

			}

			PtGen.pt(132);
			// projet.g:82:77: ( consts )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==29) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// projet.g:82:77: consts
					{
					pushFollow(FOLLOW_consts_in_decproc496);
					consts();
					state._fsp--;

					}
					break;

			}

			// projet.g:82:85: ( vars )?
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==55) ) {
				alt21=1;
			}
			switch (alt21) {
				case 1 :
					// projet.g:82:85: vars
					{
					pushFollow(FOLLOW_vars_in_decproc499);
					vars();
					state._fsp--;

					}
					break;

			}

			pushFollow(FOLLOW_corps_in_decproc502);
			corps();
			state._fsp--;

			PtGen.pt(133);
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "decproc"



	// $ANTLR start "ptvg"
	// projet.g:85:1: ptvg : ( ';' |);
	public final void ptvg() throws RecognitionException {
		try {
			// projet.g:85:7: ( ';' |)
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==18) ) {
				alt22=1;
			}
			else if ( (LA22_0==EOF||LA22_0==ID||LA22_0==27||(LA22_0 >= 29 && LA22_0 <= 30)||LA22_0==34||LA22_0==48||LA22_0==50||LA22_0==55) ) {
				alt22=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 22, 0, input);
				throw nvae;
			}

			switch (alt22) {
				case 1 :
					// projet.g:85:9: ';'
					{
					match(input,18,FOLLOW_18_in_ptvg518); 
					}
					break;
				case 2 :
					// projet.g:87:3: 
					{
					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ptvg"



	// $ANTLR start "corps"
	// projet.g:89:1: corps : 'debut' instructions 'fin' ;
	public final void corps() throws RecognitionException {
		try {
			// projet.g:89:7: ( 'debut' instructions 'fin' )
			// projet.g:89:9: 'debut' instructions 'fin'
			{
			match(input,30,FOLLOW_30_in_corps536); 
			PtGen.pt(7);
			pushFollow(FOLLOW_instructions_in_corps540);
			instructions();
			state._fsp--;

			match(input,40,FOLLOW_40_in_corps542); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "corps"



	// $ANTLR start "parfixe"
	// projet.g:92:1: parfixe : 'fixe' '(' pf ( ';' pf )* ')' ;
	public final void parfixe() throws RecognitionException {
		try {
			// projet.g:92:8: ( 'fixe' '(' pf ( ';' pf )* ')' )
			// projet.g:92:10: 'fixe' '(' pf ( ';' pf )* ')'
			{
			match(input,41,FOLLOW_41_in_parfixe554); 
			match(input,10,FOLLOW_10_in_parfixe556); 
			pushFollow(FOLLOW_pf_in_parfixe558);
			pf();
			state._fsp--;

			// projet.g:92:24: ( ';' pf )*
			loop23:
			while (true) {
				int alt23=2;
				int LA23_0 = input.LA(1);
				if ( (LA23_0==18) ) {
					alt23=1;
				}

				switch (alt23) {
				case 1 :
					// projet.g:92:26: ';' pf
					{
					match(input,18,FOLLOW_18_in_parfixe562); 
					pushFollow(FOLLOW_pf_in_parfixe564);
					pf();
					state._fsp--;

					}
					break;

				default :
					break loop23;
				}
			}

			match(input,11,FOLLOW_11_in_parfixe568); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "parfixe"



	// $ANTLR start "pf"
	// projet.g:95:1: pf : type ident ( ',' ident )* ;
	public final void pf() throws RecognitionException {
		try {
			// projet.g:95:5: ( type ident ( ',' ident )* )
			// projet.g:95:7: type ident ( ',' ident )*
			{
			pushFollow(FOLLOW_type_in_pf582);
			type();
			state._fsp--;

			pushFollow(FOLLOW_ident_in_pf584);
			ident();
			state._fsp--;

			PtGen.pt(141);
			// projet.g:95:35: ( ',' ident )*
			loop24:
			while (true) {
				int alt24=2;
				int LA24_0 = input.LA(1);
				if ( (LA24_0==14) ) {
					alt24=1;
				}

				switch (alt24) {
				case 1 :
					// projet.g:95:37: ',' ident
					{
					match(input,14,FOLLOW_14_in_pf590); 
					pushFollow(FOLLOW_ident_in_pf592);
					ident();
					state._fsp--;

					PtGen.pt(141);
					}
					break;

				default :
					break loop24;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "pf"



	// $ANTLR start "parmod"
	// projet.g:98:1: parmod : 'mod' '(' pm ( ';' pm )* ')' ;
	public final void parmod() throws RecognitionException {
		try {
			// projet.g:98:9: ( 'mod' '(' pm ( ';' pm )* ')' )
			// projet.g:98:11: 'mod' '(' pm ( ';' pm )* ')'
			{
			match(input,44,FOLLOW_44_in_parmod611); 
			match(input,10,FOLLOW_10_in_parmod613); 
			pushFollow(FOLLOW_pm_in_parmod615);
			pm();
			state._fsp--;

			// projet.g:98:24: ( ';' pm )*
			loop25:
			while (true) {
				int alt25=2;
				int LA25_0 = input.LA(1);
				if ( (LA25_0==18) ) {
					alt25=1;
				}

				switch (alt25) {
				case 1 :
					// projet.g:98:26: ';' pm
					{
					match(input,18,FOLLOW_18_in_parmod619); 
					pushFollow(FOLLOW_pm_in_parmod621);
					pm();
					state._fsp--;

					}
					break;

				default :
					break loop25;
				}
			}

			match(input,11,FOLLOW_11_in_parmod625); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "parmod"



	// $ANTLR start "pm"
	// projet.g:101:1: pm : type ident ( ',' ident )* ;
	public final void pm() throws RecognitionException {
		try {
			// projet.g:101:5: ( type ident ( ',' ident )* )
			// projet.g:101:7: type ident ( ',' ident )*
			{
			pushFollow(FOLLOW_type_in_pm639);
			type();
			state._fsp--;

			pushFollow(FOLLOW_ident_in_pm641);
			ident();
			state._fsp--;

			PtGen.pt(142);
			// projet.g:101:35: ( ',' ident )*
			loop26:
			while (true) {
				int alt26=2;
				int LA26_0 = input.LA(1);
				if ( (LA26_0==14) ) {
					alt26=1;
				}

				switch (alt26) {
				case 1 :
					// projet.g:101:37: ',' ident
					{
					match(input,14,FOLLOW_14_in_pm647); 
					pushFollow(FOLLOW_ident_in_pm649);
					ident();
					state._fsp--;

					PtGen.pt(142);
					}
					break;

				default :
					break loop26;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "pm"



	// $ANTLR start "instructions"
	// projet.g:104:1: instructions : instruction ( ';' instruction )* ;
	public final void instructions() throws RecognitionException {
		try {
			// projet.g:105:3: ( instruction ( ';' instruction )* )
			// projet.g:105:5: instruction ( ';' instruction )*
			{
			pushFollow(FOLLOW_instruction_in_instructions669);
			instruction();
			state._fsp--;

			// projet.g:105:17: ( ';' instruction )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( (LA27_0==18) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// projet.g:105:19: ';' instruction
					{
					match(input,18,FOLLOW_18_in_instructions673); 
					pushFollow(FOLLOW_instruction_in_instructions675);
					instruction();
					state._fsp--;

					}
					break;

				default :
					break loop27;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "instructions"



	// $ANTLR start "instruction"
	// projet.g:108:1: instruction : ( inssi | inscond | boucle | lecture | ecriture | affouappel |);
	public final void instruction() throws RecognitionException {
		try {
			// projet.g:109:3: ( inssi | inscond | boucle | lecture | ecriture | affouappel |)
			int alt28=7;
			switch ( input.LA(1) ) {
			case 51:
				{
				alt28=1;
				}
				break;
			case 28:
				{
				alt28=2;
				}
				break;
			case 54:
				{
				alt28=3;
				}
				break;
			case 43:
				{
				alt28=4;
				}
				break;
			case 33:
				{
				alt28=5;
				}
				break;
			case ID:
				{
				alt28=6;
				}
				break;
			case 14:
			case 18:
			case 26:
			case 37:
			case 39:
			case 40:
			case 42:
			case 52:
			case 53:
				{
				alt28=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 28, 0, input);
				throw nvae;
			}
			switch (alt28) {
				case 1 :
					// projet.g:109:5: inssi
					{
					pushFollow(FOLLOW_inssi_in_instruction692);
					inssi();
					state._fsp--;

					}
					break;
				case 2 :
					// projet.g:110:5: inscond
					{
					pushFollow(FOLLOW_inscond_in_instruction698);
					inscond();
					state._fsp--;

					}
					break;
				case 3 :
					// projet.g:111:5: boucle
					{
					pushFollow(FOLLOW_boucle_in_instruction704);
					boucle();
					state._fsp--;

					}
					break;
				case 4 :
					// projet.g:112:5: lecture
					{
					pushFollow(FOLLOW_lecture_in_instruction710);
					lecture();
					state._fsp--;

					}
					break;
				case 5 :
					// projet.g:113:5: ecriture
					{
					pushFollow(FOLLOW_ecriture_in_instruction716);
					ecriture();
					state._fsp--;

					}
					break;
				case 6 :
					// projet.g:114:5: affouappel
					{
					pushFollow(FOLLOW_affouappel_in_instruction722);
					affouappel();
					state._fsp--;

					}
					break;
				case 7 :
					// projet.g:116:3: 
					{
					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "instruction"



	// $ANTLR start "inssi"
	// projet.g:118:1: inssi : 'si' expression 'alors' instructions ( 'sinonsi' expression 'alors' instructions )* ( 'sinon' instructions )? 'fsi' ;
	public final void inssi() throws RecognitionException {
		try {
			// projet.g:118:7: ( 'si' expression 'alors' instructions ( 'sinonsi' expression 'alors' instructions )* ( 'sinon' instructions )? 'fsi' )
			// projet.g:118:9: 'si' expression 'alors' instructions ( 'sinonsi' expression 'alors' instructions )* ( 'sinon' instructions )? 'fsi'
			{
			match(input,51,FOLLOW_51_in_inssi740); 
			PtGen.pt(50);
			pushFollow(FOLLOW_expression_in_inssi743);
			expression();
			state._fsp--;

			PtGen.pt(12); PtGen.pt(51);
			match(input,25,FOLLOW_25_in_inssi747); 
			pushFollow(FOLLOW_instructions_in_inssi749);
			instructions();
			state._fsp--;

			PtGen.pt(52);
			// projet.g:119:3: ( 'sinonsi' expression 'alors' instructions )*
			loop29:
			while (true) {
				int alt29=2;
				int LA29_0 = input.LA(1);
				if ( (LA29_0==53) ) {
					alt29=1;
				}

				switch (alt29) {
				case 1 :
					// projet.g:119:4: 'sinonsi' expression 'alors' instructions
					{
					PtGen.pt(53);
					match(input,53,FOLLOW_53_in_inssi758); 
					pushFollow(FOLLOW_expression_in_inssi760);
					expression();
					state._fsp--;

					PtGen.pt(12); PtGen.pt(51);
					match(input,25,FOLLOW_25_in_inssi764); 
					pushFollow(FOLLOW_instructions_in_inssi766);
					instructions();
					state._fsp--;

					PtGen.pt(52);
					}
					break;

				default :
					break loop29;
				}
			}

			// projet.g:120:3: ( 'sinon' instructions )?
			int alt30=2;
			int LA30_0 = input.LA(1);
			if ( (LA30_0==52) ) {
				alt30=1;
			}
			switch (alt30) {
				case 1 :
					// projet.g:120:4: 'sinon' instructions
					{
					PtGen.pt(53);
					match(input,52,FOLLOW_52_in_inssi777); 
					pushFollow(FOLLOW_instructions_in_inssi779);
					instructions();
					state._fsp--;

					}
					break;

			}

			PtGen.pt(54);
			match(input,42,FOLLOW_42_in_inssi786); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "inssi"



	// $ANTLR start "inscond"
	// projet.g:123:1: inscond : 'cond' expression ':' instructions ( ',' expression ':' instructions )* ( 'aut' instructions |) 'fcond' ;
	public final void inscond() throws RecognitionException {
		try {
			// projet.g:123:9: ( 'cond' expression ':' instructions ( ',' expression ':' instructions )* ( 'aut' instructions |) 'fcond' )
			// projet.g:123:11: 'cond' expression ':' instructions ( ',' expression ':' instructions )* ( 'aut' instructions |) 'fcond'
			{
			match(input,28,FOLLOW_28_in_inscond800); 
			PtGen.pt(63);
			pushFollow(FOLLOW_expression_in_inscond804);
			expression();
			state._fsp--;

			PtGen.pt(12); PtGen.pt(61);
			match(input,16,FOLLOW_16_in_inscond808); 
			pushFollow(FOLLOW_instructions_in_inscond810);
			instructions();
			state._fsp--;

			PtGen.pt(62);
			// projet.g:124:11: ( ',' expression ':' instructions )*
			loop31:
			while (true) {
				int alt31=2;
				int LA31_0 = input.LA(1);
				if ( (LA31_0==14) ) {
					alt31=1;
				}

				switch (alt31) {
				case 1 :
					// projet.g:124:12: ',' expression ':' instructions
					{
					match(input,14,FOLLOW_14_in_inscond825); 
					PtGen.pt(64);
					pushFollow(FOLLOW_expression_in_inscond829);
					expression();
					state._fsp--;

					PtGen.pt(12); PtGen.pt(61);
					match(input,16,FOLLOW_16_in_inscond833); 
					pushFollow(FOLLOW_instructions_in_inscond835);
					instructions();
					state._fsp--;

					PtGen.pt(62);
					}
					break;

				default :
					break loop31;
				}
			}

			// projet.g:125:11: ( 'aut' instructions |)
			int alt32=2;
			int LA32_0 = input.LA(1);
			if ( (LA32_0==26) ) {
				alt32=1;
			}
			else if ( (LA32_0==39) ) {
				alt32=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 32, 0, input);
				throw nvae;
			}

			switch (alt32) {
				case 1 :
					// projet.g:125:12: 'aut' instructions
					{
					PtGen.pt(64);
					match(input,26,FOLLOW_26_in_inscond856); 
					pushFollow(FOLLOW_instructions_in_inscond859);
					instructions();
					state._fsp--;

					}
					break;
				case 2 :
					// projet.g:125:51: 
					{
					}
					break;

			}

			PtGen.pt(65);
			match(input,39,FOLLOW_39_in_inscond879); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "inscond"



	// $ANTLR start "boucle"
	// projet.g:129:1: boucle : 'ttq' expression 'faire' instructions 'fait' ;
	public final void boucle() throws RecognitionException {
		try {
			// projet.g:129:9: ( 'ttq' expression 'faire' instructions 'fait' )
			// projet.g:129:11: 'ttq' expression 'faire' instructions 'fait'
			{
			match(input,54,FOLLOW_54_in_boucle893); 
			PtGen.pt(71);
			pushFollow(FOLLOW_expression_in_boucle897);
			expression();
			state._fsp--;

			PtGen.pt(12); PtGen.pt(72);
			match(input,36,FOLLOW_36_in_boucle901); 
			pushFollow(FOLLOW_instructions_in_boucle903);
			instructions();
			state._fsp--;

			PtGen.pt(73);
			match(input,37,FOLLOW_37_in_boucle907); 
			PtGen.pt(74);
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "boucle"



	// $ANTLR start "lecture"
	// projet.g:132:1: lecture : 'lire' '(' ident ( ',' ident )* ')' ;
	public final void lecture() throws RecognitionException {
		try {
			// projet.g:132:8: ( 'lire' '(' ident ( ',' ident )* ')' )
			// projet.g:132:10: 'lire' '(' ident ( ',' ident )* ')'
			{
			match(input,43,FOLLOW_43_in_lecture922); 
			match(input,10,FOLLOW_10_in_lecture924); 
			pushFollow(FOLLOW_ident_in_lecture926);
			ident();
			state._fsp--;

			PtGen.pt(31);
			// projet.g:132:43: ( ',' ident )*
			loop33:
			while (true) {
				int alt33=2;
				int LA33_0 = input.LA(1);
				if ( (LA33_0==14) ) {
					alt33=1;
				}

				switch (alt33) {
				case 1 :
					// projet.g:132:45: ',' ident
					{
					match(input,14,FOLLOW_14_in_lecture932); 
					pushFollow(FOLLOW_ident_in_lecture934);
					ident();
					state._fsp--;

					PtGen.pt(31);
					}
					break;

				default :
					break loop33;
				}
			}

			match(input,11,FOLLOW_11_in_lecture941); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "lecture"



	// $ANTLR start "ecriture"
	// projet.g:135:1: ecriture : 'ecrire' '(' expression ( ',' expression )* ')' ;
	public final void ecriture() throws RecognitionException {
		try {
			// projet.g:135:9: ( 'ecrire' '(' expression ( ',' expression )* ')' )
			// projet.g:135:11: 'ecrire' '(' expression ( ',' expression )* ')'
			{
			match(input,33,FOLLOW_33_in_ecriture954); 
			match(input,10,FOLLOW_10_in_ecriture956); 
			pushFollow(FOLLOW_expression_in_ecriture958);
			expression();
			state._fsp--;

			PtGen.pt(41);
			// projet.g:135:51: ( ',' expression )*
			loop34:
			while (true) {
				int alt34=2;
				int LA34_0 = input.LA(1);
				if ( (LA34_0==14) ) {
					alt34=1;
				}

				switch (alt34) {
				case 1 :
					// projet.g:135:53: ',' expression
					{
					match(input,14,FOLLOW_14_in_ecriture964); 
					pushFollow(FOLLOW_expression_in_ecriture966);
					expression();
					state._fsp--;

					PtGen.pt(41);
					}
					break;

				default :
					break loop34;
				}
			}

			match(input,11,FOLLOW_11_in_ecriture973); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ecriture"



	// $ANTLR start "affouappel"
	// projet.g:138:1: affouappel : ident ( ':=' expression | ( effixes ( effmods )? )? ) ;
	public final void affouappel() throws RecognitionException {
		try {
			// projet.g:139:3: ( ident ( ':=' expression | ( effixes ( effmods )? )? ) )
			// projet.g:139:5: ident ( ':=' expression | ( effixes ( effmods )? )? )
			{
			pushFollow(FOLLOW_ident_in_affouappel989);
			ident();
			state._fsp--;

			// projet.g:139:12: ( ':=' expression | ( effixes ( effmods )? )? )
			int alt37=2;
			int LA37_0 = input.LA(1);
			if ( (LA37_0==17) ) {
				alt37=1;
			}
			else if ( (LA37_0==10||LA37_0==14||LA37_0==18||LA37_0==26||LA37_0==37||(LA37_0 >= 39 && LA37_0 <= 40)||LA37_0==42||(LA37_0 >= 52 && LA37_0 <= 53)) ) {
				alt37=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 37, 0, input);
				throw nvae;
			}

			switch (alt37) {
				case 1 :
					// projet.g:139:14: ':=' expression
					{
					PtGen.pt(21);
					match(input,17,FOLLOW_17_in_affouappel996); 
					pushFollow(FOLLOW_expression_in_affouappel998);
					expression();
					state._fsp--;

					PtGen.pt(22);
					}
					break;
				case 2 :
					// projet.g:140:16: ( effixes ( effmods )? )?
					{
					PtGen.pt(151);
					// projet.g:140:33: ( effixes ( effmods )? )?
					int alt36=2;
					int LA36_0 = input.LA(1);
					if ( (LA36_0==10) ) {
						alt36=1;
					}
					switch (alt36) {
						case 1 :
							// projet.g:140:34: effixes ( effmods )?
							{
							pushFollow(FOLLOW_effixes_in_affouappel1020);
							effixes();
							state._fsp--;

							PtGen.pt(162);
							// projet.g:140:59: ( effmods )?
							int alt35=2;
							int LA35_0 = input.LA(1);
							if ( (LA35_0==10) ) {
								alt35=1;
							}
							switch (alt35) {
								case 1 :
									// projet.g:140:60: effmods
									{
									pushFollow(FOLLOW_effmods_in_affouappel1025);
									effmods();
									state._fsp--;

									PtGen.pt(162);
									}
									break;

							}

							}
							break;

					}

					PtGen.pt(152);
					}
					break;

			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "affouappel"



	// $ANTLR start "effixes"
	// projet.g:144:1: effixes : '(' ( expression ( ',' expression )* )? ')' ;
	public final void effixes() throws RecognitionException {
		try {
			// projet.g:144:9: ( '(' ( expression ( ',' expression )* )? ')' )
			// projet.g:144:11: '(' ( expression ( ',' expression )* )? ')'
			{
			match(input,10,FOLLOW_10_in_effixes1061); 
			// projet.g:144:15: ( expression ( ',' expression )* )?
			int alt39=2;
			int LA39_0 = input.LA(1);
			if ( ((LA39_0 >= ID && LA39_0 <= INT)||LA39_0==10||LA39_0==13||LA39_0==15||LA39_0==38||LA39_0==46||LA39_0==56) ) {
				alt39=1;
			}
			switch (alt39) {
				case 1 :
					// projet.g:144:16: expression ( ',' expression )*
					{
					pushFollow(FOLLOW_expression_in_effixes1064);
					expression();
					state._fsp--;

					// projet.g:144:28: ( ',' expression )*
					loop38:
					while (true) {
						int alt38=2;
						int LA38_0 = input.LA(1);
						if ( (LA38_0==14) ) {
							alt38=1;
						}

						switch (alt38) {
						case 1 :
							// projet.g:144:29: ',' expression
							{
							match(input,14,FOLLOW_14_in_effixes1068); 
							pushFollow(FOLLOW_expression_in_effixes1070);
							expression();
							state._fsp--;

							}
							break;

						default :
							break loop38;
						}
					}

					}
					break;

			}

			match(input,11,FOLLOW_11_in_effixes1078); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "effixes"



	// $ANTLR start "effmods"
	// projet.g:147:1: effmods : '(' ( ident ( ',' ident )* )? ')' ;
	public final void effmods() throws RecognitionException {
		try {
			// projet.g:147:9: ( '(' ( ident ( ',' ident )* )? ')' )
			// projet.g:147:10: '(' ( ident ( ',' ident )* )? ')'
			{
			match(input,10,FOLLOW_10_in_effmods1090); 
			// projet.g:147:14: ( ident ( ',' ident )* )?
			int alt41=2;
			int LA41_0 = input.LA(1);
			if ( (LA41_0==ID) ) {
				alt41=1;
			}
			switch (alt41) {
				case 1 :
					// projet.g:147:15: ident ( ',' ident )*
					{
					pushFollow(FOLLOW_ident_in_effmods1093);
					ident();
					state._fsp--;

					PtGen.pt(161);
					// projet.g:147:38: ( ',' ident )*
					loop40:
					while (true) {
						int alt40=2;
						int LA40_0 = input.LA(1);
						if ( (LA40_0==14) ) {
							alt40=1;
						}

						switch (alt40) {
						case 1 :
							// projet.g:147:39: ',' ident
							{
							match(input,14,FOLLOW_14_in_effmods1098); 
							pushFollow(FOLLOW_ident_in_effmods1100);
							ident();
							state._fsp--;

							PtGen.pt(161);
							}
							break;

						default :
							break loop40;
						}
					}

					}
					break;

			}

			match(input,11,FOLLOW_11_in_effmods1109); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "effmods"



	// $ANTLR start "expression"
	// projet.g:150:1: expression : ( exp1 ) ( 'ou' exp1 )* ;
	public final void expression() throws RecognitionException {
		try {
			// projet.g:150:11: ( ( exp1 ) ( 'ou' exp1 )* )
			// projet.g:150:13: ( exp1 ) ( 'ou' exp1 )*
			{
			// projet.g:150:13: ( exp1 )
			// projet.g:150:14: exp1
			{
			pushFollow(FOLLOW_exp1_in_expression1123);
			exp1();
			state._fsp--;

			}

			// projet.g:150:20: ( 'ou' exp1 )*
			loop42:
			while (true) {
				int alt42=2;
				int LA42_0 = input.LA(1);
				if ( (LA42_0==47) ) {
					alt42=1;
				}

				switch (alt42) {
				case 1 :
					// projet.g:150:21: 'ou' exp1
					{
					match(input,47,FOLLOW_47_in_expression1127); 
					PtGen.pt(12);
					pushFollow(FOLLOW_exp1_in_expression1131);
					exp1();
					state._fsp--;

					PtGen.pt(12); PtGen.pt(113);
					}
					break;

				default :
					break loop42;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "expression"



	// $ANTLR start "exp1"
	// projet.g:153:1: exp1 : exp2 ( 'et' exp2 )* ;
	public final void exp1() throws RecognitionException {
		try {
			// projet.g:153:7: ( exp2 ( 'et' exp2 )* )
			// projet.g:153:9: exp2 ( 'et' exp2 )*
			{
			pushFollow(FOLLOW_exp2_in_exp11150);
			exp2();
			state._fsp--;

			// projet.g:153:14: ( 'et' exp2 )*
			loop43:
			while (true) {
				int alt43=2;
				int LA43_0 = input.LA(1);
				if ( (LA43_0==35) ) {
					alt43=1;
				}

				switch (alt43) {
				case 1 :
					// projet.g:153:15: 'et' exp2
					{
					match(input,35,FOLLOW_35_in_exp11153); 
					PtGen.pt(12);
					pushFollow(FOLLOW_exp2_in_exp11157);
					exp2();
					state._fsp--;

					PtGen.pt(12); PtGen.pt(112);
					}
					break;

				default :
					break loop43;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp1"



	// $ANTLR start "exp2"
	// projet.g:156:1: exp2 : ( 'non' exp2 | exp3 );
	public final void exp2() throws RecognitionException {
		try {
			// projet.g:156:7: ( 'non' exp2 | exp3 )
			int alt44=2;
			int LA44_0 = input.LA(1);
			if ( (LA44_0==46) ) {
				alt44=1;
			}
			else if ( ((LA44_0 >= ID && LA44_0 <= INT)||LA44_0==10||LA44_0==13||LA44_0==15||LA44_0==38||LA44_0==56) ) {
				alt44=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 44, 0, input);
				throw nvae;
			}

			switch (alt44) {
				case 1 :
					// projet.g:156:9: 'non' exp2
					{
					match(input,46,FOLLOW_46_in_exp21176); 
					pushFollow(FOLLOW_exp2_in_exp21178);
					exp2();
					state._fsp--;

					PtGen.pt(12);
					PtGen.pt(111);
					}
					break;
				case 2 :
					// projet.g:157:5: exp3
					{
					pushFollow(FOLLOW_exp3_in_exp21188);
					exp3();
					state._fsp--;

					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp2"



	// $ANTLR start "exp3"
	// projet.g:160:1: exp3 : exp4 ( '=' exp4 | '<>' exp4 | '>' exp4 | '>=' exp4 | '<' exp4 | '<=' exp4 )? ;
	public final void exp3() throws RecognitionException {
		try {
			// projet.g:160:7: ( exp4 ( '=' exp4 | '<>' exp4 | '>' exp4 | '>=' exp4 | '<' exp4 | '<=' exp4 )? )
			// projet.g:160:9: exp4 ( '=' exp4 | '<>' exp4 | '>' exp4 | '>=' exp4 | '<' exp4 | '<=' exp4 )?
			{
			pushFollow(FOLLOW_exp4_in_exp31204);
			exp4();
			state._fsp--;

			// projet.g:161:3: ( '=' exp4 | '<>' exp4 | '>' exp4 | '>=' exp4 | '<' exp4 | '<=' exp4 )?
			int alt45=7;
			switch ( input.LA(1) ) {
				case 22:
					{
					alt45=1;
					}
					break;
				case 21:
					{
					alt45=2;
					}
					break;
				case 23:
					{
					alt45=3;
					}
					break;
				case 24:
					{
					alt45=4;
					}
					break;
				case 19:
					{
					alt45=5;
					}
					break;
				case 20:
					{
					alt45=6;
					}
					break;
			}
			switch (alt45) {
				case 1 :
					// projet.g:161:5: '=' exp4
					{
					match(input,22,FOLLOW_22_in_exp31211); 
					PtGen.pt(13);
					pushFollow(FOLLOW_exp4_in_exp31216);
					exp4();
					state._fsp--;

					PtGen.pt(14); PtGen.pt(106);
					}
					break;
				case 2 :
					// projet.g:162:5: '<>' exp4
					{
					match(input,21,FOLLOW_21_in_exp31224); 
					PtGen.pt(13);
					pushFollow(FOLLOW_exp4_in_exp31228);
					exp4();
					state._fsp--;

					PtGen.pt(14); PtGen.pt(105);
					}
					break;
				case 3 :
					// projet.g:163:5: '>' exp4
					{
					match(input,23,FOLLOW_23_in_exp31236); 
					PtGen.pt(11);
					pushFollow(FOLLOW_exp4_in_exp31241);
					exp4();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(104);
					}
					break;
				case 4 :
					// projet.g:164:5: '>=' exp4
					{
					match(input,24,FOLLOW_24_in_exp31249); 
					PtGen.pt(11);
					pushFollow(FOLLOW_exp4_in_exp31253);
					exp4();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(103);
					}
					break;
				case 5 :
					// projet.g:165:5: '<' exp4
					{
					match(input,19,FOLLOW_19_in_exp31261); 
					PtGen.pt(11);
					pushFollow(FOLLOW_exp4_in_exp31266);
					exp4();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(102);
					}
					break;
				case 6 :
					// projet.g:166:5: '<=' exp4
					{
					match(input,20,FOLLOW_20_in_exp31274); 
					PtGen.pt(11);
					pushFollow(FOLLOW_exp4_in_exp31278);
					exp4();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(101);
					}
					break;

			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp3"



	// $ANTLR start "exp4"
	// projet.g:170:1: exp4 : exp5 ( '+' exp5 | '-' exp5 )* ;
	public final void exp4() throws RecognitionException {
		try {
			// projet.g:170:7: ( exp5 ( '+' exp5 | '-' exp5 )* )
			// projet.g:170:9: exp5 ( '+' exp5 | '-' exp5 )*
			{
			pushFollow(FOLLOW_exp5_in_exp41301);
			exp5();
			state._fsp--;

			// projet.g:171:9: ( '+' exp5 | '-' exp5 )*
			loop46:
			while (true) {
				int alt46=3;
				int LA46_0 = input.LA(1);
				if ( (LA46_0==13) ) {
					alt46=1;
				}
				else if ( (LA46_0==15) ) {
					alt46=2;
				}

				switch (alt46) {
				case 1 :
					// projet.g:171:10: '+' exp5
					{
					match(input,13,FOLLOW_13_in_exp41313); 
					PtGen.pt(11);
					pushFollow(FOLLOW_exp5_in_exp41318);
					exp5();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(94);
					}
					break;
				case 2 :
					// projet.g:172:10: '-' exp5
					{
					match(input,15,FOLLOW_15_in_exp41331); 
					PtGen.pt(11);
					pushFollow(FOLLOW_exp5_in_exp41336);
					exp5();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(93);
					}
					break;

				default :
					break loop46;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp4"



	// $ANTLR start "exp5"
	// projet.g:176:1: exp5 : primaire ( '*' primaire | 'div' primaire )* ;
	public final void exp5() throws RecognitionException {
		try {
			// projet.g:176:7: ( primaire ( '*' primaire | 'div' primaire )* )
			// projet.g:176:9: primaire ( '*' primaire | 'div' primaire )*
			{
			pushFollow(FOLLOW_primaire_in_exp51363);
			primaire();
			state._fsp--;

			// projet.g:177:9: ( '*' primaire | 'div' primaire )*
			loop47:
			while (true) {
				int alt47=3;
				int LA47_0 = input.LA(1);
				if ( (LA47_0==12) ) {
					alt47=1;
				}
				else if ( (LA47_0==32) ) {
					alt47=2;
				}

				switch (alt47) {
				case 1 :
					// projet.g:177:14: '*' primaire
					{
					match(input,12,FOLLOW_12_in_exp51379); 
					PtGen.pt(11);
					pushFollow(FOLLOW_primaire_in_exp51383);
					primaire();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(92);
					}
					break;
				case 2 :
					// projet.g:178:13: 'div' primaire
					{
					match(input,32,FOLLOW_32_in_exp51399); 
					PtGen.pt(11);
					pushFollow(FOLLOW_primaire_in_exp51404);
					primaire();
					state._fsp--;

					PtGen.pt(11); PtGen.pt(91);
					}
					break;

				default :
					break loop47;
				}
			}

			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "exp5"



	// $ANTLR start "primaire"
	// projet.g:182:1: primaire : ( valeur | ident | '(' expression ')' );
	public final void primaire() throws RecognitionException {
		try {
			// projet.g:182:9: ( valeur | ident | '(' expression ')' )
			int alt48=3;
			switch ( input.LA(1) ) {
			case INT:
			case 13:
			case 15:
			case 38:
			case 56:
				{
				alt48=1;
				}
				break;
			case ID:
				{
				alt48=2;
				}
				break;
			case 10:
				{
				alt48=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 48, 0, input);
				throw nvae;
			}
			switch (alt48) {
				case 1 :
					// projet.g:182:11: valeur
					{
					pushFollow(FOLLOW_valeur_in_primaire1429);
					valeur();
					state._fsp--;

					}
					break;
				case 2 :
					// projet.g:183:5: ident
					{
					pushFollow(FOLLOW_ident_in_primaire1436);
					ident();
					state._fsp--;

					PtGen.pt(85);
					}
					break;
				case 3 :
					// projet.g:184:5: '(' expression ')'
					{
					match(input,10,FOLLOW_10_in_primaire1445); 
					pushFollow(FOLLOW_expression_in_primaire1448);
					expression();
					state._fsp--;

					match(input,11,FOLLOW_11_in_primaire1450); 
					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "primaire"



	// $ANTLR start "valeur"
	// projet.g:187:1: valeur : ( nbentier | '+' nbentier | '-' nbentier | 'vrai' | 'faux' );
	public final void valeur() throws RecognitionException {
		try {
			// projet.g:187:9: ( nbentier | '+' nbentier | '-' nbentier | 'vrai' | 'faux' )
			int alt49=5;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt49=1;
				}
				break;
			case 13:
				{
				alt49=2;
				}
				break;
			case 15:
				{
				alt49=3;
				}
				break;
			case 56:
				{
				alt49=4;
				}
				break;
			case 38:
				{
				alt49=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 49, 0, input);
				throw nvae;
			}
			switch (alt49) {
				case 1 :
					// projet.g:187:11: nbentier
					{
					pushFollow(FOLLOW_nbentier_in_valeur1464);
					nbentier();
					state._fsp--;

					PtGen.pt(84);
					}
					break;
				case 2 :
					// projet.g:188:5: '+' nbentier
					{
					match(input,13,FOLLOW_13_in_valeur1472); 
					pushFollow(FOLLOW_nbentier_in_valeur1474);
					nbentier();
					state._fsp--;

					PtGen.pt(84);
					}
					break;
				case 3 :
					// projet.g:189:5: '-' nbentier
					{
					match(input,15,FOLLOW_15_in_valeur1482); 
					pushFollow(FOLLOW_nbentier_in_valeur1484);
					nbentier();
					state._fsp--;

					PtGen.pt(83);
					}
					break;
				case 4 :
					// projet.g:190:5: 'vrai'
					{
					match(input,56,FOLLOW_56_in_valeur1492); 
					PtGen.pt(82);
					}
					break;
				case 5 :
					// projet.g:191:5: 'faux'
					{
					match(input,38,FOLLOW_38_in_valeur1500); 
					PtGen.pt(81);
					}
					break;

			}
		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "valeur"



	// $ANTLR start "nbentier"
	// projet.g:203:1: nbentier : INT ;
	public final void nbentier() throws RecognitionException {
		Token INT1=null;

		try {
			// projet.g:203:11: ( INT )
			// projet.g:203:15: INT
			{
			INT1=(Token)match(input,INT,FOLLOW_INT_in_nbentier1535); 
			 UtilLex.valEnt = Integer.parseInt((INT1!=null?INT1.getText():null));
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "nbentier"



	// $ANTLR start "ident"
	// projet.g:205:1: ident : ID ;
	public final void ident() throws RecognitionException {
		Token ID2=null;

		try {
			// projet.g:205:7: ( ID )
			// projet.g:205:9: ID
			{
			ID2=(Token)match(input,ID,FOLLOW_ID_in_ident1546); 
			 UtilLex.traiterId((ID2!=null?ID2.getText():null)); 
			}

		}

		catch (RecognitionException e) {reportError (e) ; throw e ; }
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ident"

	// Delegated rules



	public static final BitSet FOLLOW_unitprog_in_unite64 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_unite66 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unitmodule_in_unite80 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_unite82 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_49_in_unitprog98 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_unitprog100 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_16_in_unitprog102 = new BitSet(new long[]{0x00850000E0000000L});
	public static final BitSet FOLLOW_declarations_in_unitprog117 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_corps_in_unitprog126 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_45_in_unitmodule145 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_unitmodule147 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_16_in_unitmodule149 = new BitSet(new long[]{0x00850000A0000000L});
	public static final BitSet FOLLOW_declarations_in_unitmodule163 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_partiedef_in_declarations181 = new BitSet(new long[]{0x0085000020000002L});
	public static final BitSet FOLLOW_partieref_in_declarations184 = new BitSet(new long[]{0x0081000020000002L});
	public static final BitSet FOLLOW_consts_in_declarations187 = new BitSet(new long[]{0x0081000000000002L});
	public static final BitSet FOLLOW_vars_in_declarations190 = new BitSet(new long[]{0x0001000000000002L});
	public static final BitSet FOLLOW_decprocs_in_declarations193 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_31_in_partiedef210 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_partiedef212 = new BitSet(new long[]{0x0000000000044000L});
	public static final BitSet FOLLOW_14_in_partiedef217 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_partiedef219 = new BitSet(new long[]{0x0000000000044000L});
	public static final BitSet FOLLOW_ptvg_in_partiedef226 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_50_in_partieref238 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_specif_in_partieref241 = new BitSet(new long[]{0x0000000000044000L});
	public static final BitSet FOLLOW_14_in_partieref246 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_specif_in_partieref248 = new BitSet(new long[]{0x0000000000044000L});
	public static final BitSet FOLLOW_ptvg_in_partieref255 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ident_in_specif269 = new BitSet(new long[]{0x0000120000000002L});
	public static final BitSet FOLLOW_41_in_specif275 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_specif277 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_type_in_specif279 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_14_in_specif285 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_type_in_specif287 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_11_in_specif294 = new BitSet(new long[]{0x0000100000000002L});
	public static final BitSet FOLLOW_44_in_specif319 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_specif322 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_type_in_specif324 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_14_in_specif330 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_type_in_specif332 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_11_in_specif339 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_29_in_consts357 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_consts361 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_22_in_consts365 = new BitSet(new long[]{0x010000400000A040L});
	public static final BitSet FOLLOW_valeur_in_consts367 = new BitSet(new long[]{0x0000000000040020L});
	public static final BitSet FOLLOW_ptvg_in_consts371 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_55_in_vars390 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_type_in_vars394 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_vars396 = new BitSet(new long[]{0x0000000408044000L});
	public static final BitSet FOLLOW_14_in_vars402 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_vars405 = new BitSet(new long[]{0x0000000408044000L});
	public static final BitSet FOLLOW_ptvg_in_vars412 = new BitSet(new long[]{0x0000000408000002L});
	public static final BitSet FOLLOW_34_in_type432 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_27_in_type444 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_decproc_in_decprocs461 = new BitSet(new long[]{0x0001000000040000L});
	public static final BitSet FOLLOW_ptvg_in_decprocs463 = new BitSet(new long[]{0x0001000000000002L});
	public static final BitSet FOLLOW_48_in_decproc481 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_decproc484 = new BitSet(new long[]{0x0080120060000000L});
	public static final BitSet FOLLOW_parfixe_in_decproc488 = new BitSet(new long[]{0x0080100060000000L});
	public static final BitSet FOLLOW_parmod_in_decproc491 = new BitSet(new long[]{0x0080000060000000L});
	public static final BitSet FOLLOW_consts_in_decproc496 = new BitSet(new long[]{0x0080000040000000L});
	public static final BitSet FOLLOW_vars_in_decproc499 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_corps_in_decproc502 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_18_in_ptvg518 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_30_in_corps536 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_corps540 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_40_in_corps542 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_41_in_parfixe554 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_parfixe556 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_pf_in_parfixe558 = new BitSet(new long[]{0x0000000000040800L});
	public static final BitSet FOLLOW_18_in_parfixe562 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_pf_in_parfixe564 = new BitSet(new long[]{0x0000000000040800L});
	public static final BitSet FOLLOW_11_in_parfixe568 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_pf582 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_pf584 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_14_in_pf590 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_pf592 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_44_in_parmod611 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_parmod613 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_pm_in_parmod615 = new BitSet(new long[]{0x0000000000040800L});
	public static final BitSet FOLLOW_18_in_parmod619 = new BitSet(new long[]{0x0000000408000000L});
	public static final BitSet FOLLOW_pm_in_parmod621 = new BitSet(new long[]{0x0000000000040800L});
	public static final BitSet FOLLOW_11_in_parmod625 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_type_in_pm639 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_pm641 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_14_in_pm647 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_pm649 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_instruction_in_instructions669 = new BitSet(new long[]{0x0000000000040002L});
	public static final BitSet FOLLOW_18_in_instructions673 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instruction_in_instructions675 = new BitSet(new long[]{0x0000000000040002L});
	public static final BitSet FOLLOW_inssi_in_instruction692 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_inscond_in_instruction698 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_boucle_in_instruction704 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_lecture_in_instruction710 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ecriture_in_instruction716 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_affouappel_in_instruction722 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_51_in_inssi740 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_inssi743 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_25_in_inssi747 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_inssi749 = new BitSet(new long[]{0x0030040000000000L});
	public static final BitSet FOLLOW_53_in_inssi758 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_inssi760 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_25_in_inssi764 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_inssi766 = new BitSet(new long[]{0x0030040000000000L});
	public static final BitSet FOLLOW_52_in_inssi777 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_inssi779 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_42_in_inssi786 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_28_in_inscond800 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_inscond804 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_16_in_inscond808 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_inscond810 = new BitSet(new long[]{0x0000008004004000L});
	public static final BitSet FOLLOW_14_in_inscond825 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_inscond829 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_16_in_inscond833 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_inscond835 = new BitSet(new long[]{0x0000008004004000L});
	public static final BitSet FOLLOW_26_in_inscond856 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_inscond859 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_39_in_inscond879 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_54_in_boucle893 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_boucle897 = new BitSet(new long[]{0x0000001000000000L});
	public static final BitSet FOLLOW_36_in_boucle901 = new BitSet(new long[]{0x0048080210040020L});
	public static final BitSet FOLLOW_instructions_in_boucle903 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_37_in_boucle907 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_43_in_lecture922 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_lecture924 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_lecture926 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_14_in_lecture932 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_lecture934 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_11_in_lecture941 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_33_in_ecriture954 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_10_in_ecriture956 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_ecriture958 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_14_in_ecriture964 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_ecriture966 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_11_in_ecriture973 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ident_in_affouappel989 = new BitSet(new long[]{0x0000000000020402L});
	public static final BitSet FOLLOW_17_in_affouappel996 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_affouappel998 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_effixes_in_affouappel1020 = new BitSet(new long[]{0x0000000000000402L});
	public static final BitSet FOLLOW_effmods_in_affouappel1025 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_10_in_effixes1061 = new BitSet(new long[]{0x010040400000AC60L});
	public static final BitSet FOLLOW_expression_in_effixes1064 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_14_in_effixes1068 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_effixes1070 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_11_in_effixes1078 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_10_in_effmods1090 = new BitSet(new long[]{0x0000000000000820L});
	public static final BitSet FOLLOW_ident_in_effmods1093 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_14_in_effmods1098 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ident_in_effmods1100 = new BitSet(new long[]{0x0000000000004800L});
	public static final BitSet FOLLOW_11_in_effmods1109 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exp1_in_expression1123 = new BitSet(new long[]{0x0000800000000002L});
	public static final BitSet FOLLOW_47_in_expression1127 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_exp1_in_expression1131 = new BitSet(new long[]{0x0000800000000002L});
	public static final BitSet FOLLOW_exp2_in_exp11150 = new BitSet(new long[]{0x0000000800000002L});
	public static final BitSet FOLLOW_35_in_exp11153 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_exp2_in_exp11157 = new BitSet(new long[]{0x0000000800000002L});
	public static final BitSet FOLLOW_46_in_exp21176 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_exp2_in_exp21178 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exp3_in_exp21188 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exp4_in_exp31204 = new BitSet(new long[]{0x0000000001F80002L});
	public static final BitSet FOLLOW_22_in_exp31211 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp4_in_exp31216 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_21_in_exp31224 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp4_in_exp31228 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_23_in_exp31236 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp4_in_exp31241 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_24_in_exp31249 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp4_in_exp31253 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_19_in_exp31261 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp4_in_exp31266 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_20_in_exp31274 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp4_in_exp31278 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exp5_in_exp41301 = new BitSet(new long[]{0x000000000000A002L});
	public static final BitSet FOLLOW_13_in_exp41313 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp5_in_exp41318 = new BitSet(new long[]{0x000000000000A002L});
	public static final BitSet FOLLOW_15_in_exp41331 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_exp5_in_exp41336 = new BitSet(new long[]{0x000000000000A002L});
	public static final BitSet FOLLOW_primaire_in_exp51363 = new BitSet(new long[]{0x0000000100001002L});
	public static final BitSet FOLLOW_12_in_exp51379 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_primaire_in_exp51383 = new BitSet(new long[]{0x0000000100001002L});
	public static final BitSet FOLLOW_32_in_exp51399 = new BitSet(new long[]{0x010000400000A460L});
	public static final BitSet FOLLOW_primaire_in_exp51404 = new BitSet(new long[]{0x0000000100001002L});
	public static final BitSet FOLLOW_valeur_in_primaire1429 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ident_in_primaire1436 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_10_in_primaire1445 = new BitSet(new long[]{0x010040400000A460L});
	public static final BitSet FOLLOW_expression_in_primaire1448 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_11_in_primaire1450 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_nbentier_in_valeur1464 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_13_in_valeur1472 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_nbentier_in_valeur1474 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_15_in_valeur1482 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_nbentier_in_valeur1484 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_valeur1492 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_38_in_valeur1500 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_nbentier1535 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_ident1546 = new BitSet(new long[]{0x0000000000000002L});
}
